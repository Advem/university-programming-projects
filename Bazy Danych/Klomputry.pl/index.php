<?php
    
    session_start();

    $servername = "localhost";
    $username = "root";
    $password = "";

    try
    {
        $conn = new PDO("mysql:host=$servername;dbname=klomputry", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    
    catch(PDOException $e)
    {
        echo "Connection failed: " . $e->getMessage();    
    }

    if(isset($_GET['url']) && $_GET['url'] == "home")
    {
        $_GET['url'] == NULL;
    }

?>

<!DOCTYPE html>
<html lang = "pl">

<head>

    <meta charset = "utf-8"/>
    <title>Klomputry.pl - internetowy sklep dla graczy</title>
    <meta name = "description" content = " Tutaj jakiś opis jakże wspaniałego sklepu internetowego." />
    <script type="text/javascript" src="script.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/css/iziToast.min.css">
    <link rel = "stylesheet" href = "Fontello/css/fontello.css" type = "text/css" />
    <link href = "https://fonts.googleapis.com/css?family=Raleway:300,400,500,600&amp;subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox-1.3.4.css">
    <script src="fancybox/jquery.fancybox-1.3.4.js"></script>
    <link rel = "stylesheet" type = "text/css" href = "style.css"/>
    

</head>

<body>
    
    <header>
    
        <section id = "upper_menu">
            <div id = "logo" onclick = "window.location.href = 'index.php';"></div>
            <div id = "logging">
               
                <?php
                
                    if(isset($_SESSION['isLogged']))
                    {
                        if($_SESSION['isLogged'] == true)
                        {
                            echo "<h2>Witaj,</h2>";
                            echo "<h3>".$_SESSION['nick']."</h3>";
                            
                            if(isset($_SESSION['role']) &&  $_SESSION['role'] == 1)
                            {
                                echo "<div id = \"pages\" onclick = \"window.location.href = 'index.php?url=pages';\"><i class = \"icon-check\"></i>Pages</div>";
                                echo "<div id = \"profile\" onclick = \"window.location.href = 'index.php?url=admin';\"><i class = \"icon-users\"></i>Users</div>";
                                // echo "<div id = \"profile\" onclick = \"window.location.href = 'index.php?url=profile';\"><i class = \"icon-user\"></i>Profile</div>";
                                echo "<div id = \"products\" onclick = \"window.location.href = 'index.php?url=products';\"><i class = \"icon-search\"></i>Products</div>";
                                echo "<div id = \"orders\" onclick = \"window.location.href = 'index.php?url=orders';\"><i class = \"icon-down-bold\"></i>Orders</div>";
                                // echo "<div id = \"history\" onclick = \"window.location.href = 'index.php?url=history';\"><i class = \"icon-down-bold\"></i>History</div>";
                                echo "<div id = \"cart\" onclick = \"window.location.href = 'index.php?url=cart';\"><i class = \"icon-basket\"></i>Checkout</div>";
                                // echo "<div id = \"gallery\" onclick = \"window.location.href = 'index.php?url=gallery';\"><i class = \"icon-plus-squared\"></i>Gallery</div>";
                            }
                            
                            else
                            {
                                echo "<div id = \"profile\" onclick = \"window.location.href = 'index.php?url=profile';\"><i class = \"icon-user\"></i>Profile</div>";
                                echo "<div id = \"cart\" onclick = \"window.location.href = 'index.php?url=cart';\"><i class = \"icon-basket\"></i>Checkout</div>";
                                echo "<div id = \"history\" onclick = \"window.location.href = 'index.php?url=history';\"><i class = \"icon-down-bold\"></i>Orders</div>";
                                echo "<div id = \"gallery\" onclick = \"window.location.href = 'index.php?url=gallery';\"><i class = \"icon-check\"></i>Gallery</div>";
                            }
                            
                            echo "<div onclick = \"window.location.href = 'logout.php';\"><i class = \"icon-block\"></i>Log Out</div>"; 
                        }
                    }
                
                    else 
                    {
                        echo "<div onclick = \"window.location.href = 'index.php?url=login';\"><i class = \"icon-lock-open\"></i>Log In</div> 
                        <div onclick = \"window.location.href = 'index.php?url=signup';\"><i class = \"icon-user-add\"></i>Sign Up</div>";
                    }
                ?>
                </div>  
            
        </section>
    
        <section id = "bg_image"><div id = "bg_image_cover"></div></section>
        
        <section id = "lower_menu">

        <?php

                $sql = "SELECT * FROM mypage";  
                $stmt = $conn->prepare($sql);
                $stmt->execute();
                if($stmt->rowCount() > 0)
                {
                    while($row = $stmt->fetch())
                    {
                        echo'<div id="'.$row['title'].'" style="text-transform: lowercase; text-transform: capitalize;" onclick = "window.location.href = \'index.php?url='.$row['title'].'\';">'.$row['title'].'</div>';
                    }
                }
        ?>
            <!-- <div id = "news" onclick = "window.location.href = 'index.php?url=news';">Nowości</div>
            <div id = "sales" onclick = "window.location.href = 'index.php?url=sales';">Promocje</div>
            <div id = "bestseller" onclick = "window.location.href = 'index.php?url=bestseller';">Bestseller</div>
            <div id = "gaming" onclick = "window.location.href = 'index.php?url=gaming';">Gaming</div>
            <div id = "electronics" onclick = "window.location.href = 'index.php?url=electronics';">Elektronika</div>
            <div id = "softwares" onclick = "window.location.href = 'index.php?url=softwares';">Oprogramowanie</div>
            <div id = "merchanies" onclick = "window.location.href = 'index.php?url=merchanies';">Gadżety</div> -->

                    

        </section>
        
    </header>
       
    <main>
        <section id = "front_page">
           
           <?php
            
            if(isset($_GET['url']))
            {   
                if($_GET['url'] == "login")
                {
                    echo    
                    '<form method = "post" action = "login.php">
                    <div id = login_field>
                    <br><h3>Zaloguj się!</h3><br>
                
                    Login:<br>
                    <input placeholder = "" type = "text" name = "userlogin" id = "formlogin" autocomplete = "off"/><br>
                    
                    Hasło:<br>
                    <input placeholder = "" type = "password" name = "userpassword" id = "formpassword" autocomplete = "off"/><br>
                    
                    <div id = "error-login-message" style = "color: red;"></div><br>
                    <input type = "submit" name = "usersubmit" value = "Enter"/></div>  
                    </form>';
                    
                 if(isset($_GET['value']) && $_GET['value'] == "error-login")  
                 {
                    echo '<script>
                    error_login();
                    </script>'; 
                 }
                    
                }
                
                else if($_GET['url'] == "signup")
                {
                    echo 
                    '<form method = "post" action = "signup.php">
                    <div id = login_field>
                    <br><h3>Nie posiadasz konta? - Stwórz je!</h3>
                    <br>
                    ';
                
                    echo
                    '
                    <br>
                    <input placeholder = " Email" type = "email" name = "useremail" id = "formemail" autocomplete = "off"/><br>
                    
                    <br>
                    <input placeholder = " Login (conajmniej 5 znaków)" type = "text" name = "userlogin" id = "formlogin" autocomplete = "off"/><br>
                    
                    <br>
                    <input placeholder = " Hasło (conajmniej 5 znaków)" type = "password" name = "userpassword" id = "formpassword" autocomplete = "off"/><br>
                    
                    <br>
                    <input placeholder = " Powtórz hasło" type = "password" name = "userpasswordconfirm" id = "formpasswordconfirm" autocomplete = "off"/><br>
                    
                    <div id = "error-signup-email-message" style = "color: red;">
                    </div>
                    
                    <div id = "error-signup-login1-message" style = "color: red;">
                    </div>
                    
                    <div id = "error-signup-login2-message" style = "color: red;">
                    </div>
                    
                    <div id = "error-signup-password1-message" style = "color: red;">
                    </div>
                    
                    <div id = "error-signup-password2-message" style = "color: red;">
                    </div>
                    
                    <br>
                    <input type = "submit" name = "usersubmit" value = "Enter"/>
                
                    </div>
                    </form>';
                    
                    if(isset($_GET['value']) && $_GET['value'] == "error-signup-email")  
                 {
                    echo '<script>
                    error_signup_email();
                    </script>'; 
                 }
                    
                    else if(isset($_GET['value']) && $_GET['value'] == "error-signup-login1")  
                 {
                    echo '<script>
                    error_signup_login1();
                    </script>'; 
                 }
                    
                    else if(isset($_GET['value']) && $_GET['value'] == "error-signup-login2")  
                 {
                    echo '<script>
                    error_signup_login2();
                    </script>'; 
                 }
                    
                    else if(isset($_GET['value']) && $_GET['value'] == "error-signup-password1")  
                 {
                    echo '<script>
                    error_signup_password1();
                    </script>'; 
                 }
                    
                     else if(isset($_GET['value']) && $_GET['value'] == "error-signup-password2")  
                 {
                    echo '<script>
                    error_signup_password2();
                    </script>'; 
                 }
                    
                    
            }


// SUB PAGES + PRODUCTS

                else {

                    $q = "SELECT * FROM mypage";

                    $stm = $conn->prepare($q);
                    $stm->execute();

                    if($stm->rowCount() > 0)
                        {
                        
                            while($row2 = $stm->fetch())
                            {
                                    if($_GET['url'] == $row2['title'])
                                    {
                                    $sql = "SELECT * FROM myproduct WHERE category = :id";
                                    
                                    
                                    $stmt = $conn->prepare($sql);
                                    $stmt -> bindParam(":id", $row2['id_mypage']);
                                    $stmt->execute();
                                    
                                    echo '<div class = display-products>';
                                    
                                    if($stmt->rowCount() > 0)
                                    {
                                        while($row = $stmt->fetch())
                                        {
                                            echo '
                                                <table class = "display-product">
                                                    <tr>
                                                        <th><img style = "max-width: 90px; max-height: 85px; margin: 5px;" src = "'.$row["image"].'"\></th>
                                                        <th>' . $row["price"] . ' PLN</th>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <a target="_blank" href="'.$row["image"].'">' . $row["title"] . '</a>
                                                        </td>
                                                    </tr>
                                                    
                                                    <tr>
                                                        <td><span>'. $row['description'] .'</span></td>
                                                    </tr>
                                                    <tr>
                                                ';
                                            
                                            if(isset($_SESSION['isLogged']) && $_SESSION['isLogged'] == true)
                                            {
                                                echo '<td><div class = "mybutton2" onclick = "cartadd('.$row['id_myproduct'].');"><i class = "icon-down-bold"></i>KUP</div></td>';
                                            }
                                            
                                            else 
                                            {
                                                echo '<td><div class = "mybutton2" onclick = "cartadd_error();"><i class = "icon-down-bold"></i>KUP</div></td>';
                                            }
                                            
                                            echo '</tr></table>';
                                        }
                                    }
                                    
                                    echo '</div>';
                                
                            }
                        }
                        
                    }
                    
                    else {}
                
                    }
            }
            
// HOME PAGE
            
            else
                {
                    $sql = "SELECT * FROM myproduct WHERE category = 99";
                    $stmt = $conn->prepare($sql);
                    $stmt->execute();
                
                    if($stmt->rowCount() > 0)
                    {
                        while($row = $stmt->fetch())
                        {
                            // title musi byc np "slide?" i katergoria = 7 !!!!!!!!!
                            echo '
                                <slider>
                                    <slide id = "' . $row["title"] . '" style = "background-image: url(' . $row["image"]. ');" alt = "banner"><p></p></slide>
                                </slider>
                            ';
                        }
                    }
                
                            echo '<div id = "welcome"><div>';
                
                                if(isset($_SESSION['isLogged']) && $_SESSION['isLogged'] == true)
                                {
                                    echo "<h3>Witaj, ".$_SESSION['nick']." na Kłomputry.pl !</h3>";
                                }
                
                                else 
                                {
                                    echo "<h3>Witaj na Kłomputry.pl !</h3>";
                                }
                
                               echo '
                                        <br>Zapraszamy Cie, drogi użytkowniku do zapoznania się z naszym serwisem - Kłomputry.pl, gdzie znajdziesz ogrom interesujących produktów, takie jak: komputery gamingowe, laptopy, konsole, gry wideo, myszki, klawiatury, słuchawki oraz wiele innych artykułów, a to wszystko w bardzo atrakcyjnych cenach. Życzymy udanych zakupów!
                                    
                                    <h5>
                                        <br>Pamiętaj o wypełnieniu formularza z danymi osobami po założeniu konta. Znajdziesz tę opcję klikając
                                        przycisk "Profile" na górnym pasku menu tuż po zalogowaniu się na swoje konto.
                                    </h5>
                                </div>
                            </div>
                            ';
                
                        echo '
                            <div id = "logo-animation"><div>
                        
                        ';
                }
             
            
                if(isset($_GET['url']) && isset($_SESSION['isLogged']) && $_SESSION['isLogged'] == true)
            {
                
            }

            if(isset($_GET['url']) && isset($_SESSION['isLogged']) && $_SESSION['isLogged'] == true)
            {
            
                    if($_GET['url'] == "profile")
                    {
                        
                        echo '<div id = profile_field>';
                        echo '<br><h3>Zaktualizuj dane swojego konta - '.$_SESSION['nick'].'</h3>';
                        
                        $q = "SELECT firstname, surname, address, city, birthdate, phone FROM myuser WHERE login = :userlogin ;";
                        $stm = $conn->prepare($q);
                        $stm->bindParam(":userlogin", $_SESSION['nick']);
                        $stm->execute();
                        
                        $userdata = $stm->fetch();
                        
                        echo 
                        "
                            <br><br>
                            <div class = \"profile-data\" id = \"firstname-data\" style = \"visibility: visible;\">
                            <div>Imię:</div>
                            <div id = \"firstname-show-data\">".$userdata['firstname']."</div>   
                            
                            <div class = \"profile-data\" id = \"firstname-change\" style = \"visibility: visible;\">
                            <input type = \"text\" id = \"firstname-input\" autocomplete = \"off\"/>
                            <div class = \"mybutton\" onclick = \"change_data('firstname','change', 'data');\">Wyślij</div>
                                    
                            </div>
                            </div>
                        ";
                        
                        echo 
                        "
                            <br><br>
                            <div class = \"profile-data\" id = \"surname-data\" style = \"visibility: visible;\">
                            <div>Nazwisko:  </div> 
                            <div id = \"surname-show-data\">".$userdata['surname']."</div>        
                            
                            <div class = \"profile-data\" id = \"surname-change\" style = \"visibility: visible;\">
                            <input type = \"text\" id = \"surname-input\" autocomplete = \"off\"/>
                            <div class = \"mybutton\" onclick = \"change_data('surname','change', 'data');\">Wyślij</div>
                            
                            </div>
                            </div>
                        ";
                        
                        echo 
                        "
                            <br><br>
                            <div class = \"profile-data\" id = \"address-data\" style = \"visibility: visible;\">
                            <div>Adres:  </div>
                            <div id = \"address-show-data\">".$userdata['address']."</div>        
                            
                            <div class = \"profile-data\" id = \"address-change\" style = \"visibility: visible;\">
                            <input type = \"text\" id = \"address-input\" autocomplete = \"off\"/>
                            <div class = \"mybutton\" onclick = \"change_data('address','change', 'data');\">Wyślij</div>
                            
                            </div>
                            </div>
                        ";
                        
                        echo 
                        "
                            <br><br>
                            <div class = \"profile-data\" id = \"city-data\" style = \"visibility: visible;\">
                            <div>Miasto:  </div>
                            <div id = \"city-show-data\">".$userdata['city']."</div>        
                            
                            <div class = \"profile-data\" id = \"city-change\" style = \"visibility: visible;\">
                            <input type = \"text\" id = \"city-input\" autocomplete = \"off\"/>
                            <div class = \"mybutton\" onclick = \"change_data('city','change', 'data');\">Wyślij</div>
                            
                            </div>
                            </div>
                        ";
                        
                        echo 
                        "
                            <br><br>
                            <div class = \"profile-data\" id = \"phone-data\" style = \"visibility: visible;\">
                            <div>Telefon:  </div>
                            <div id = \"phone-show-data\">".$userdata['phone']."</div>        
                        
                            <div class = \"profile-data\" id = \"phone-change\" style = \"visibility: visible;\">
                            <input type = \"number\" id = \"phone-input\" autocomplete = \"off\"/>
                            <div class = \"mybutton\" onclick = \"change_data('phone','change', 'data');\">Wyślij</div>
                            
                            </div>
                            </div>
                        ";
                        
                        echo 
                        "
                            <br><br>
                            <div class = \"profile-data\" id = \"birthdate-data\" style = \"visibility: visible;\">
                            <div>Data urodzenia:  </div>
                            <div id = \"birthdate-show-data\">".$userdata['birthdate']."</div>        
                            
                            <div class = \"profile-data\" id = \"birthdate-change\" style = \"visibility: visible;\">
                            <input type = \"date\" id = \"birthdate-input\" autocomplete = \"off\"/>
                            <div class = \"mybutton\" onclick = \"change_data('birthdate','change', 'data');\">Wyślij</div>
                            
                            </div>
                            </div>
                        ";
                    
                        echo '</div>';
                    }
                    
                    else if($_GET['url'] == "cart")
                    {
                        
                            if(!empty($_SESSION["shopping_cart"]))  
                            {  
                                
                                echo '
                                <br><br><h3 align="center"><p>Szczegóły zamówienia</p></h3><br>  
                                
                                <table class="cart">  
                                <tr align="left">
                                        <th width="1%">
                                        <th width="5%"></th>
                                        <th width="25%">Nazwa</th>    
                                        <th width="50%">Informacje</th>   
                                        <th width="10%" align="center">Cena</th>  
                                        <th width="5%" align="center">Usuń</th>
                                        <th width="1%">
                                </tr>
                                
                                <tr>
                                        <td>.</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                </tr>
                                ';
                                
                                $total = 0.0001;  
                                foreach($_SESSION["shopping_cart"] as $keys => $values)  
                                {  
                        echo'  
                            <tr>  
                                    <td></td>
                                    <td><a style="text-decoration: none;" target="_blank" href="'.$values["item_image"].'"><img style = "max-width: 72px;" src = '.$values["item_image"].'></a></td>
                                    <td>'.$values["item_name"].'</td>  
                                    <td>'.$values["item_description"].'</td>
                                    <td align="center">'.$values["item_price"].' PLN</td>  
                                    
                                <td align="center"><a href="function.php?action=delete&id='.$values["item_id"].'"><i class = "icon-cancel" style = "color: black; text-align: center; font-size: 20px;"></i></a></td>
                                <td></td>
                            </tr>  
                        ';  
                
                                        $total = $total + $values["item_price"];
                                }  
                        echo '  
                            <tr>  
                                <td colspan="4" align="right">Suma: </td>  
                                <td colspan="1" align="center"><h4>';
                                
                                echo number_format($total,2);
                                
                        echo  ' PLN</h4></td>  
                                
                            </tr>
                            </table>
                            
                            <table style = "margin-bottom: 40px;">
                            <th align="center"><div class = "mybutton" onclick = "cartorder('.$total.');"><i class = "icon-check"></i>Zamawiam</div></th>
                            </table>
                        ';
                            }
                        else echo '<br><br><h3 align="center"><p>Twój koszyk jest pusty</p></h3>';
                    }

                    else if($_GET['url'] == "gallery")
                    {
                        $sql = "SELECT * FROM myproduct";
                        //ORDER BY purchase_time DESC
                        $stmt = $conn->prepare($sql);
                        $stmt->execute();
                        
                        echo '<div class="gallery">';
                        if($stmt->rowCount() > 0)
                        {
                            while($row = $stmt->fetch())
                            {
                                $imageThumbURL = $row["title"];
                                    $imageURL = $row["image"];
                            echo'
                                
                                <a href="'. $imageURL.'" data-fancybox="gallery" data-caption="'.$row["title"].'" >
                                    <img src="'.$imageURL.'" alt="" />
                                </a>
                                
                                ';
                            }
                        }
                        echo '</div>';
                    }
                    
                    else if($_GET['url'] == "history")
                    {
                        echo '<br><br><h3 align = "center"><p>Historia zamówień<p></h3>';
                        
                        echo '
                                    <br><br><table style = "margin-bottom: 40px;">
                                        <tr align="left">
                                            <td width="1%"></th>
                                            <th width="8%"></th>
                                            <th width="18%">Numer (ID)</th> 
                                            <th width="33%">Produkt (ID)</th>   
                                            <th width="6%" align="right">Kwota</th>
                                            <th width="7%" align="right">Suma</th>
                                            <th width="14%" align="center">Data</th>
                                            <th width="8%">Status</th>
                                            <th width="2%" align="center"></th>
                                        </tr>
                                    
                                ';
                        
                        $sql = "SELECT * FROM myorder INNER JOIN myuser ON myorder.buyer=myuser.id_myuser INNER JOIN myproduct ON myorder.product=myproduct.id_myproduct WHERE myuser.login = :nick ORDER BY purchase_time DESC;";
                        //ORDER BY purchase_time DESC
                        $stmt = $conn->prepare($sql);
                        $stmt->bindParam(":nick", $_SESSION['nick']);
                        $stmt->execute();
                        if($stmt->rowCount() > 0)
                        {
                            while($row = $stmt->fetch())
                            {
                                echo'
                                        
                                        <tr align="left" style="font-size: 12px;"> 
                                            <td></td>
                                            <td><a style="text-decoration: none;" target="_blank" href="'.$row["image"].'"><img style = "max-width: 72px;" src = '.$row['image'].'></a></td>
                                            <td>'.$row['package'].' ('.$row['id_myorder'].')</td>
                                            <td>'.$row['title'].' ('.$row['product'].')</td>
                                            <td align="right">'.$row['price'].' zł </td>
                                            <td align="right">'.$row['total'].' zł</td>
                                            <td align="center">'.$row['purchase_time'].'</td>
                                        ';
                                
                                if($row['order_status'] == 0) echo'<td style = "color: #5d5d5d;">oczekiwanie';
                                else if($row['order_status'] == -1) echo'<td style = "color: red;">anulowano';
                                else if($row['order_status'] == 1) echo'<td style = "color: #ffc400;">przygotowanie';
                                else if($row['order_status'] == 2) echo'<td style = "color: #00d000;"d>wysłano';
                                else if($row['order_status'] == 3) echo'<td style = "color: #0093ff;">dostarczono';
                                else echo '<td style = "color: #ff00d8;">błąd statusu '.$row['order_status'];
                                
                                echo '<td></td></tr>';
                            }
            
                            echo '</table>';
                        }
                    }
               
            }
            
            if(isset($_GET['url']) && isset($_SESSION['isLogged']) && $_SESSION['isLogged'] == true && isset($_SESSION['role']) &&  $_SESSION['role'] == 1)
            {
                if($_GET['url'] == "admin")
                {
                    echo '<div id = profile_field>';
                    echo '<h3><p>Panel Administratora</p></h3>';
                    echo 
                    "
                    <div class = \"user-data\">
                    <input placeholder = \"Login użytkownika\" type = \"text\" id = \"searchuser\" autocomplete = \"off\"/>
                    <div id = \"admin-buttons\">
                    <div class = \"mybutton\" onclick = \"find_user();\"><i class = \"icon-search\"></i>Szukaj</div>
                    <div class = \"mybutton\" onclick = \"delete_user();\"><i class = \"icon-block\"></i>Usuń</div>
                    </div>
                    <div id = \"profile-info\"></div>
                    </div>
                    ";
                }
                
                else if($_GET['url'] == "products")
                {
                    echo '';
                    echo  "
                        <div class = \"search-box\">
                            <br><br><h3><p>Panel zarządzania produktami<p></h3>
                            <input type=\"text\" placeholder = \"Szukaj produktu...\" autocomplete = \"off\"/ id = \"searchproduct\">
                                <div id = \"admin-buttons\"> 
                                    <div class = \"mybutton\" onclick = \"window.location.href = 'index.php?url=products-edit';\"><i class = \"icon-check\"></i> Edytuj</div>
                                    <div class = \"mybutton\" onclick = \"window.location.href = 'index.php?url=products-add';\"><i class = \"icon-plus-squared\"></i> Dodaj</div>
                                    <div class = \"mybutton\" onclick = \"delete_product();\"><i class = \"icon-cancel-squared\"></i> Usuń</div>
                                </div>
                                <div class = \"result\"></div>
                        </div>
                        ";
                }
                
                else if($_GET['url'] == "products-edit")
                {
                    
                    $q = "SELECT title, id_myproduct, description, price, image, category FROM myproduct WHERE title = :name ;";
                    $stm = $conn->prepare($q);
                    $stm->bindParam(":name", $_GET['title']);
                    $stm->execute();
                    
                    $productdata = $stm->fetch();
                    
                    echo  "
                        <div class = \"search-box\">
                            <br><br><h3><p>Zaktualizuj dane instniejącego produktu<p></h3>
                            <input type=\"text\" placeholder = \"Szukaj produktu...\" autocomplete = \"off\"/ id = \"searchproduct\">
                                <div id = \"admin-buttons\"> 
                                    <div class = \"mybutton\" onclick = \"window.location.href = 'index.php?url=products-edit';\"><i class = \"icon-check\"></i> Edytuj</div>
                                    <div class = \"mybutton\" onclick = \"window.location.href = 'index.php?url=products-add';\"><i class = \"icon-plus-squared\"></i> Dodaj</div>
                                    <div class = \"mybutton\" onclick = \"delete_product();\"><i class = \"icon-cancel-squared\"></i> Usuń</div>
                                </div>
                                    
                                    
                        <br><br>
                           
                        <div class = \"product-data\" id = \"title-change\" style = \"visibility: visible;\">
                        <input type = \"text\" id = \"title-input\" autocomplete = \"off\" placeholder = \"Nazwa\"/>
                        <div class = \"mybutton\" onclick = \"change_product_data('title','change', 'data');\">Wyślij</div>
                        </div>
                        
                        <div class = \"product-data\" id = \"description-change\" style = \"visibility: visible;\">
                        <input type = \"text\" id = \"description-input\" autocomplete = \"off\" placeholder = \"Opis\"/>
                        <div class = \"mybutton\" onclick = \"change_product_data('description','change', 'data');\">Wyślij</div>
                        </div>
                                                
                        <div class = \"product-data\" id = \"price-change\" style = \"visibility: visible;\">
                        <input type = \"number\" id = \"price-input\" autocomplete = \"off\" placeholder = \"Cena\"/>
                        <div class = \"mybutton\" onclick = \"change_product_data('price','change', 'data');\">Wyślij</div>
                        </div>
                        
                        <div class = \"product-data\" id = \"image-change\" style = \"visibility: visible;\">
                        <input type = \"text\" id = \"image-input\" autocomplete = \"off\" placeholder = \"Obraz\"/>
                        <div class = \"mybutton\" onclick = \"change_product_data('image','change', 'data');\">Wyślij</div>
                        </div>
                        
                        <div class = \"product-data\" id = \"category-change\" style = \"visibility: visible;\">
                        <input type = \"text\" id = \"category-input\" autocomplete = \"off\" placeholder = \"Kategoria\"/>
                        <div class = \"mybutton\" onclick = \"change_product_data('category','change', 'data');\">Wyślij</div>
                        </div>
                        
                        <div class = \"result\"></div>  
                        </div>
                        ";     
                    
                }
                
                else if($_GET['url'] == "products-add")
                {
                    
                    echo  "
                        <div class = \"search-box\">
                            <br><br><h3><p>Dodaj nowy produkt do bazy danych<p></h3>
                            <input type=\"text\" placeholder = \"Szukaj produktu...\" autocomplete = \"off\"/ id = \"searchproduct\">
                                <div id = \"admin-buttons\"> 
                                    <div class = \"mybutton\" onclick = \"window.location.href = 'index.php?url=products-edit';\"><i class = \"icon-check\"></i> Edytuj</div>
                                    <div class = \"mybutton\" onclick = \"window.location.href = 'index.php?url=products-add';\"><i class = \"icon-plus-squared\"></i> Dodaj</div>
                                    <div class = \"mybutton\" onclick = \"delete_product();\"><i class = \"icon-cancel-squared\"></i> Usuń</div>
                                </div>
                                
                        <br><br>
                           
                        <form method = \"post\" id = \"adding-product\" action = \"function.php?q=add\">
                        
                        <input type = \"text\" id = \"titleinput\" name = \"titleinput\" autocomplete = \"off\" placeholder = \"Nazwa\"/>
                        <input type = \"text\" id = \"descriptioninput\" name = \"descriptioninput\" autocomplete = \"off\" placeholder = \"Opis\"/>
                        <input type = \"number\" id = \"priceinput\" name = \"priceinput\" autocomplete = \"off\" placeholder = \"Cena\"/>
                        <input type = \"text\" id = \"imageinput\" name = \"imageinput\" autocomplete = \"off\" placeholder = \"Obraz\"/>
                        <input type = \"text\" id = \"categoryinput\" name = \"categoryinput\" autocomplete = \"off\" placeholder = \"Kategoria\"/>
                        
                        
                        <input type = \"submit\" class = \"mybutton\" value = \"Enter\"/>
                        </form>
                        ";
                            
                        if(isset($_GET['value']) && $_GET['value'] == "adderror")  
                         {
                            echo '<br><div style = "color: red;" >Błąd podczas dodawania produktu do bazy danych!</div>';

                         }

                        else if(isset($_GET['value']) && $_GET['value'] == "correct")
                        {
                            echo '<br><div style = "color: green;" >Pomyślnie dodano produkt do bazy danych</div>';
                        }
                        
                        echo "<div class = \"result\"></div>
                        </div>"; 
                }

                else if($_GET['url'] == "orders")
                {
                    echo '<br><br><h3 align = "center"><p>Archiwum zamówień<p></h3>';
                    
                    echo '
                                <br><br><table style = "margin-bottom: 40px;">
                                    <tr align="left">
                                        <th width="1%"></th>
                                        <th width="17%">Numer (ID)</th> 
                                        <th width="32%">Produkt (ID)</th>   
                                        <th width="9%">Klient (ID)</th>
                                        <th width="6%" align="right">Kwota</th>
                                        <th width="7%" align="right">Suma</th>
                                        <th width="14%" align="center">Data</th>
                                        <th width="8%">Status</th>
                                        <th width="5%" align="center"></th>
                                    </tr>
                                
                            ';
                    
                    $sql = "SELECT * FROM myorder INNER JOIN myuser ON myorder.buyer=myuser.id_myuser INNER JOIN myproduct ON myorder.product=myproduct.id_myproduct ORDER BY purchase_time DESC;";
                    //ORDER BY purchase_time DESC
                    $stmt = $conn->prepare($sql);
                    $stmt->execute();
                    if($stmt->rowCount() > 0)
                    {
                        while($row = $stmt->fetch())
                        {
                            echo'
                                    
                                    <tr align="left" style="font-size: 12px;">  
                                        <td></td>
                                        <td>'.$row['package'].' ('.$row['id_myorder'].')</td>
                                        <td>'.$row['title'].' ('.$row['product'].')</td>
                                        <td>'.$row['login'].' ('.$row['buyer'].')</td>
                                        <td align="right">'.$row['price'].' zł </td>
                                        <td align="right">'.$row['total'].' zł</td>
                                        <td align="center">'.$row['purchase_time'].'</td>
                                    ';
                            
                            if($row['order_status'] == 0) echo'<td style = "color: #5d5d5d;">oczekiwanie';
                            else if($row['order_status'] == -1) echo'<td style = "color: red;">anulowano';
                            else if($row['order_status'] == 1) echo'<td style = "color: #ffc400;">przygotowanie';
                            else if($row['order_status'] == 2) echo'<td style = "color: #00d000;"d>wysłano';
                            else if($row['order_status'] == 3) echo'<td style = "color: #0093ff;">dostarczono';
                            else echo '<td style = "color: #ff00d8;">błąd statusu '.$row['order_status'];
                            
                            echo'</td><td>
                                <span style = "color: black; font-size: 14px; margin-left: 3px; cursor: pointer;" onclick = "window.location.href = \'function.php?action=status&value=down&id='.$row['id_myorder'].'\';"><i class = "icon-left-dir"></i></span>
                                <span style = "color: black; font-size: 14px; cursor: pointer;" onclick = "window.location.href = \'function.php?action=status&value=up&id='.$row['id_myorder'].'\';"><i class = "icon-right-dir"></i></span>
                                </td>';
                            
                            echo '<td></td></tr>';
                        }
                        
                        echo '</table>';
                    }
                }

                else if($_GET['url'] == "pages")
                {
                    echo '<br><br><h3 align = "center"><p>Edycja podstron<p></h3>';
                    
                    echo '<h3><form method = "post" id = "adding-page" action = "function.php?action=addpage" align="center">
                        
                        <input type = "text" id = "titleinput" name = "title" autocomplete = "off" placeholder = "Nazwa"/>
                        <input type = "number" id = "category" name = "id" autocomplete = "off" placeholder = "Numer Kategorii"/>
                        <input type = "submit" value="Dodaj" style="font-size: 14px;">
                        <div id = "add-page-error" style = "font-size: 14px; color: red; margin-top:20px;"></div><br>
                    </form></h3>';
                    

                    if(isset($_GET['value']) && $_GET['value'] == "add-page-error")  
                 {
                    echo '<script>
                    add_page_error();
                    </script>'; 
                 }
                    
                    
                    

                    echo '
                                <br><br><table style = "margin-bottom: 40px;">
                                    <tr align="center">
                                        <th width="30%" align="center"></th>
                                        <th width="10%" align="center">ID</th>
                                        <th width="20%" align="left">Nazwa</th>
                                        <th width="10% align="left">Usuń</th>
                                        
                                    </tr>
                                
                            ';
                    
                    $sql = "SELECT * FROM mypage";
                    //ORDER BY purchase_time DESC
                    $stmt = $conn->prepare($sql);
                    $stmt->execute();
                    if($stmt->rowCount() > 0)
                    {
                        while($row = $stmt->fetch())
                        {
                            echo'
                                    
                                    <tr align="center" style="font-size: 16px;">  
                                        <td></td>
                                        <td align="center">'.$row['id_mypage'].'</td>
                                        <td align="left">'.$row['title'].'</td>
                                        <td align="center">
                                        <span style = "color: red; font-size: 14px; cursor: pointer;" onclick = "window.location.href = \'function.php?action=deletepage&id='.$row['id_mypage'].'\';"><i class = "icon-minus-squared"></i></span>
                                        </td>
                                        
                                ';
                        
                            
                            echo '<td></td></tr>';
                        }
                        
                        echo '</table>';
                        
                    }
                }
                
            }
            
            else if(isset($_GET['url']) && isset($_SESSION['role']) && $_SESSION['role'] != 1 && 
                  ($_GET['url'] == "admin" || $_GET['url'] == "products" || $_GET['url'] == "products-add" || $_GET['url'] == "products-edit" || $_GET['url'] == "orders" || $_GET['url'] == 'pages'))
            {
                echo '<br><h3 align="center"><p>Nie masz uprawnień do przeglądania tej podstrony!</p></h3>';
            }
            
            ?>    
        </section>
    </main>
    
    <footer></footer>
    
    <script type="text/javascript">
        $(document).ready(function()
        {
            $('.search-box input[type="text"]').on("keyup input", function()
            {
                var inputVal = $(this).val();
                var resultDropdown = $(this).siblings(".result");
                if(inputVal.length)
                {
                    $.get("backend-search.php", {term: inputVal}).done(function(data)
                    {
                        resultDropdown.html(data);
                    });
                } 
                else
                {
                    resultDropdown.empty();
                }
            });
                $(document).on("click", ".result p", function()
                {
                    $(this).parents(".search-   box").find('input[type="text"]').val($(this).text());
                    $(this).parent(".result").empty();
                });
        });</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/js/iziToast.min.js" type="text/javascript"></script>
    <script>
        $("[data-fancybox]").fancybox();
    </script>
    <script>
        <?php 
        if(isset($_GET['url']) && !($_GET['url'] == 'profile' || $_GET['url'] == 'products' || $_GET['url'] == 'admin' || $_GET['url'] == 'cart' || $_GET['url'] == 'orders' || $_GET['url'] == 'history' || $_GET['url'] == 'gallery' || $_GET['url'] == 'pages'))
        {
            echo "active_tab(\"".$_GET['url']."\");";   
        }
        ?>
    </script>
</body>

</html>