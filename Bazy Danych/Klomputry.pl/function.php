<?php
    
    function generateRandomString($length) 
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) 
        {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    session_start();

    $servername = "localhost";
    $username = "root";
    $password = "";

    try
    {
        $conn = new PDO("mysql:host=$servername;dbname=klomputry", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    
    catch(PDOException $e)
    {
        echo "Connection failed: " . $e->getMessage();    
    }
    
    // FIND USER FROM DATABASE
    if(isset($_GET['q']) && $_GET['q'] == "finduser" && isset($_GET['value']))
    {
        $Qcheck = "SELECT id_myuser, login, email, firstname, surname, address, city, phone, birthdate, joineddate, role FROM myuser WHERE login = :value ;";
        $prepareQcheck = $conn->prepare($Qcheck); 
        $prepareQcheck->bindParam(":value", $_GET['value']);
        $prepareQcheck->execute();
        $t = $prepareQcheck->fetch();

        if(empty($t))
        {
            echo "Nie zanleziono użytkownika ".$_GET['value'];
        }

        else
        {
            //echo "Użytkownik ".$_GET['value'];//" posiada uprawnienia: ".$t['role'];
            echo "<br><br>ID:  ".$t['id_myuser']."<br>";
            echo "<br>Login:  ".$t['login']."<br>";
            echo "<br>Email:  ".$t['email']."<br>";
            echo "<br>Imię:  ".$t['firstname']."<br>";
            echo "<br>Nazwisko:  ".$t['surname']."<br>";
            echo "<br>Adres:  ".$t['address']."<br>";
            echo "<br>Miasto:  ".$t['city']."<br>";
            echo "<br>Telefon:  ".$t['phone']."<br>";
    

            echo "<br>Data urodzin:  ".$t['birthdate']."<br>";
            echo "<br>Data dołączenia:  ".$t['joineddate']."<br>";
            echo "<br>Stopień uprawnienia:  ".$t['role']."<br>";
            
        }
    }

    // UPDATE PROFILE TO DATABASE
   if(isset($_GET['q']) && $_GET['q'] == "change" && isset($_GET['value']) && isset($_GET['what']) && isset($_SESSION['isLogged']))
   {

       $isQ = true; 
       
       switch($_GET['what'])
       {
               case "firstname": 
               $q = "UPDATE myuser SET firstname = :value WHERE login = :userlogin";      
               break;
               
               case "surname": 
               $q = "UPDATE myuser SET surname = :value WHERE login = :userlogin";      
               break;
               
               case "address": 
               $q = "UPDATE myuser SET address = :value WHERE login = :userlogin";      
               break;
               
               case "city": 
               $q = "UPDATE myuser SET city = :value WHERE login = :userlogin";      
               break;
               
               case "phone": 
               $q = "UPDATE myuser SET phone = :value WHERE login = :userlogin";      
               break;
               
               case "birthdate": 
               $q = "UPDATE myuser SET birthdate = :value WHERE login = :userlogin";      
               break;
               
           default: 
               $isQ = false;
       }
       
       if($isQ)
       { 
        $stm = $conn->prepare($q);
           
        $stm->bindParam(":value", $_GET['value']);
        $stm->bindParam(":userlogin", $_SESSION['nick']);
        $stm->execute();
       }
        
       echo $_GET['value'];
       
   }

    // UPDATE PRODUCT TO DATABASE
   if(isset($_GET['q']) && $_GET['q'] == "editproduct" && isset($_GET['value']) && isset($_GET['what']) && isset($_GET['name']) && isset($_SESSION['isLogged']))
   {

       $isQ = true; 
       
       switch($_GET['what'])
       {
               case "id_myproduct": 
               $q = "UPDATE myproduct SET id_myproduct = :value WHERE title = :name";      
               break;
               
               case "title": 
               $q = "UPDATE myproduct SET title = :value WHERE title = :name";      
               break;
               
               case "description": 
               $q = "UPDATE myproduct SET description = :value WHERE title = :name";      
               break;
               
               case "price": 
               $q = "UPDATE myproduct SET price = :value WHERE title = :name";      
               break;
               
               case "category": 
               $q = "UPDATE myproduct SET category = :value WHERE title = :name";      
               break;
               
               case "image": 
               $q = "UPDATE myproduct SET image = :value WHERE title = :name";      
               break;
               
               
               
           default: 
               $isQ = false;
       }
       
       if($isQ)
       { 
        $stm = $conn->prepare($q);
           
        $stm->bindParam(":value", $_GET['value']);
        $stm->bindParam(":name", $_GET['name']);
        $stm->execute();
       }
        
       echo $_GET['value'];
       
   }

    // DELETE USER FROM DATABASE
    if(isset($_GET['q']) && $_GET['q'] == "deleteuser" && isset($_SESSION['isLogged']) && isset($_SESSION['role']) && $_SESSION['role'] == 1 && isset($_GET['value']))
    {
        $Qcheck = "SELECT role, id_myuser FROM myuser WHERE login = :value";
        $prepareQcheck = $conn->prepare($Qcheck);
        $prepareQcheck->bindParam(":value", $_GET['value']);
        $prepareQcheck->execute();
        $t = $prepareQcheck->fetch();
        
        if(!empty($t) && $t['role'] != 1)
        {
            $Q = "DELETE FROM myuser WHERE login = :value;";
            $prepareQ = $conn->prepare($Q);
            $prepareQ->bindParam(":value", $_GET['value']);
            $prepareQ->execute();
            echo "Użytkownik ".$_GET['value']." został usunięty!";
            
        }
        
        else if(empty($t))
        {
            echo 'Nie zanleziono użytkownika '.$_GET['value'];
        }
        
        else
        {
             echo '<div style = "color: red;">Nie możesz usunąć administratora !</div>';
        }
    }

    // DELETE PRODUCT FROM DATABASE
    if(isset($_GET['q']) && $_GET['q'] == "deleteproduct" && isset($_SESSION['isLogged']) && isset($_SESSION['role']) && $_SESSION['role'] == 1 && isset($_GET['value']))
    {
        $Qcheck = "SELECT id_myproduct FROM myproduct WHERE title = :value";
        $prepareQcheck = $conn->prepare($Qcheck);
        $prepareQcheck->bindParam(":value", $_GET['value']);
        $prepareQcheck->execute();
        $t = $prepareQcheck->fetch();
        
        if(!empty($t))
        {
            $Q = "DELETE FROM myproduct WHERE title = :value;";
            $prepareQ = $conn->prepare($Q);
            $prepareQ->bindParam(":value", $_GET['value']);
            $prepareQ->execute();
            echo "Produkt ".$_GET['value']." został usunięty!";
            
        }
        
        else if(empty($t))
        {
            echo "Nie zanleziono produktu o nazwie".$_GET['value'];
        }
        
        else
        {
             echo "Nie można usunąć tego produktu!";
        }
    }

    // ADD PRODUCT TO DATABASE
    if(isset($_GET['q']) && $_GET['q'] == "add" && isset($_SESSION['isLogged']))
    {
        if (isset($_POST['titleinput']) && $_POST['titleinput'] != NULL)
        {
            $q = "INSERT INTO myproduct VALUES(NULL, :title, :description, :price, :category, :image);";
                    $stm = $conn->prepare($q);
                    $stm->bindParam(":title", $_POST['titleinput']);
                    $stm->bindParam(":description", $_POST['descriptioninput']);
                    $stm->bindParam(":price", $_POST['priceinput']);
                    $stm->bindParam(":image", $_POST['imageinput']);
                    $stm->bindParam(":category", $_POST['categoryinput']);
                    
                    //$stm->execute();
                    
                    //$productdata = $stm->fetch();
                    
                        if($stm->execute())
                        {
                            header("Location: index.php?url=products-add&value=correct");           
                        }

                        else
                        {
                            header("Location: index.php?url=products-add&value=adderror");
                        }
        }
        
        else 
        {
            header("Location: index.php?url=products-add&value=adderror");
        }
    }

    // ADD PRODUCT TO CART
    if(isset($_GET['action']) && $_GET['action'] == "add" && isset($_SESSION['isLogged']) && isset($_GET['id']))
    {
        //echo '<script type="text/javascript">window.onload = function () { alert("Dodano produkt id = '.$_GET['id'].' do koszyka."); }</script>';
        //header("Location: index.php");
        
        $Qcheck = "SELECT * FROM myproduct WHERE id_myproduct = :id;";
        $prepareQcheck = $conn->prepare($Qcheck); 
        $prepareQcheck->bindParam(":id", $_GET['id']);
        $prepareQcheck->execute();
        $row = $prepareQcheck->fetch();
        
        if(empty($row))
        {
            echo "Nie zanleziono produktu id = ".$_GET['id'];
        }
        
        if(isset($_SESSION["shopping_cart"]))  
        {  
               $item_array_id = array_column($_SESSION["shopping_cart"], "item_id");  
               //if(!in_array($_GET["id"], $item_array_id))  
               //{  
                    $count = count($_SESSION["shopping_cart"]);  
                    $item_array = array(  
                         'item_id'               =>     $_GET["id"],  
                         'item_name'             =>     $row["title"],  
                         'item_price'            =>     $row["price"],  
                         'item_description'      =>     $row["description"],
                         'item_image'            =>     $row["image"]
                    );
                   
                    $_SESSION["shopping_cart"][$count] = $item_array;  
              // } 

                //else  
               // {  
                        //echo '<script>alert("Item Already Added")</script>';  
                        //echo '<script>window.location="index.php?url=cart"</script>';  
                //}  
        } 
        
        else  
        {  
                    $item_array = array(  
                         'item_id'               =>     $_GET["id"],  
                         'item_name'             =>     $row["title"],  
                         'item_price'            =>     $row["price"],  
                         'item_description'      =>     $row["description"],
                         'item_image'            =>     $row["image"]
                    ); 
            
           $_SESSION["shopping_cart"][0] = $item_array;  
        } 
        
        //print_r($item_array);
        //echo '<script>window.location="index.php?url=cart"</script>';
    }

    // DELETE PRODUCT FROM CART
    if(isset($_GET['action']) && $_GET['action'] == "delete" && isset($_SESSION['isLogged']) && isset($_GET['id'])) 
    {
           foreach($_SESSION["shopping_cart"] as $keys => $values)  
           {  
                if($values["item_id"] == $_GET["id"])  
                {  
                     unset($_SESSION["shopping_cart"][$keys]);  
                    // echo '<script>alert("Item Removed")</script>';  
                     //echo '<script>window.location="index.php?url=cart"</script>';
                    header("Location: index.php?url=cart");
                }  
           }    
    }

    // ADD ORDER + CREATING EMAIL MESSAGE
    if(isset($_GET['action']) && $_GET['action'] == "order" && isset($_SESSION['isLogged']) && isset($_GET['total']))
    {
        
        $email = "<!DOCTYPE html><html lang = \"pl\"><head><meta charset = \"utf-8\"/><title>Kłomputry.pl - internetowy sklep dla graczy</title></head><body>";
        
        // wygenerowanie randomowej liczby jako numer paczki
        $generate = true;
        do
        {
            $package = generateRandomString(20);
            $QPack = "SELECT id_myorder FROM myorder WHERE package = :package;";
            $prepare_QPack = $conn->prepare($QPack); 
            $prepare_QPack->bindParam(":package", $package);
            $prepare_QPack->execute();
            $count = $prepare_QPack->fetchAll();
            if(count($count) == 0)
            {
                $generate = false;
                break;
            }
        } while($generate);
        
        $email .= "<div style=\"background-image: linear-gradient(white, white, white, rgba(255, 235, 172, 0.69)); max-width: 1100px; color: rgb(49, 49, 49);\"><br><div style=\"margin-top: 6px;margin-left: 400px;align-self: center;width: 300px;height: 60px;display: flex;background-size: 100%;background-position: center;background-image:url(https://i.ibb.co/vPvFjHF/main1.png);\"></div><br><p><h3 align=\"center\"> Order number: $package</h3></p><br><br>";
        
        // pobranie id usera na podstawie logniu
        $QQcheck = "SELECT * FROM myuser WHERE login = :value;";
        $prepareQQcheck = $conn->prepare($QQcheck);
        $prepareQQcheck->bindParam(":value", $_SESSION['nick']);
        $prepareQQcheck->execute();
        $t = $prepareQQcheck->fetch();
        
        if(!empty($t))
        {
            $userID = $t['id_myuser'];
            $userEmail = $t['email'];
            $email .= "<p><h4 align=\"center\">Informacje nabywcy</h4></p><br><table align=\"center\" style=\"font-size: 12px;\"><tr><th></th><th width=\"5%\">ID</th><th width=\"10%\">Login</th><th width=\"10%\">Imie</th><th width=\"10%\">Nazwisko</th><th width=\"15%\">Adres</th width=\"20%\"><th width=\"10%\">Miasto</th><th width=\"10%\">Kontakt</th><th width=\"10%\">Email</th><th></th></tr><tr align=\"center\"><td></td><td>".$t['id_myuser']."</td>><td>".$t['login']."</td><td>".$t['firstname']."</td><td>".$t['surname']."</td><td>".$t['address']."</td><td>".$t['city']."</td><td>".$t['phone']."</td><td>".$t['email']."</td><td></td></tr></table>";
           
        }
        
        else
        {
            echo 'Nie zanleziono użytkownika '.$_SESSION['nick'];
        }
        
        $email .= "<br><p><h4 align=\"center\">Zamowione produkty</h4></p><table style = \"margin-bottom: 1px;\"><tr align=\"left\"><th width=\"2%\"></th><th align =\"left\" width=\"2%\">ID</th><th width=\"30%\">Nazwa</th><th width=\"50%\">Opis</th><th width=\"10%\" align=\"right\">Cena</th><th width=\"2%\"></th></tr><tr><td>.</td></tr>";
         
        
        foreach($_SESSION["shopping_cart"] as $keys => $values)  
        {
            
            $save_value = $values["item_id"];
            
                $q = "INSERT INTO myorder VALUES(NULL, :package, LOCALTIME(), :buyer, :product, :total, 0);";
                    $stm = $conn->prepare($q);
                    $stm->bindParam(":package", $package);
                    $stm->bindParam(":buyer", $userID);
                    $stm->bindParam(":product", $values["item_id"]);
                    $stm->bindParam(":total", $_GET['total']);
                    
                    
            
            if($stm->execute())
            {
                echo "Udało się xd";
                unset($_SESSION["shopping_cart"][$keys]);
                echo '<script>toscik("","Zamówienie zostało wysłane do sprzedawcy");</script>';
            }

            else
            {
                toscik("","Niepowodzenie podczas zamówienia");
            }

            $q2 = "SELECT * FROM myproduct WHERE id_myproduct = :product2";
                    $stm2 = $conn->prepare($q2);
                    $stm2->bindParam(":product2", $save_value);
                    $stm2->execute();
                
             while($row = $stm2->fetch())
            {
                $email .= "<tr height=\"50px;\" align=\"left\" style=\"font-size: 12px;\"><td></td><td align =\"left\">".$row['id_myproduct']."</td><td>".$row['title']."</td><td>".$row['description']."</td><td align=\"right\">".$row['price']."</td><td></td></tr>";
             }

        }
        
        $total2 = $_GET['total'] - 0.0001;
        $date = new \DateTime();
        $string = $date->format(DATE_RFC2822);
        
        $email .= "<tr><td>.</td></tr><tr><td colspan=\"4\" align=\"right\"><h5>Suma:</h5></td><td colspan=\"1\" align=\"center\"><h5>".$total2." PLN</h5></td></tr> </table><br><p><h5 align=\"center\" style = \"color: rgb(31, 31, 31);\">Dziekujemy za skorzystanie z naszego sklepy Klomputry.pl. W razie pytan odnosnie naszych produktow lub formy dostawy prosimy o kontakt telefoniczny lub za pomoca poczty elektronicznej.<br><br>Ta wiadomosc zostala wygenerowana automatycznie.<br><br>".$string."</h5></p><br><br></div></body></html>";
        header("Location: sendmail.php?id=$package&output=$email&to=$userEmail");
        
        
    }

    // CHANGE STATUS
    if(isset($_GET['action']) && $_GET['action'] == "status" && isset($_SESSION['isLogged']) && isset($_GET['value']) && isset($_GET['id']))
    {
        
        $QQcheck = "SELECT order_status FROM myorder WHERE id_myorder = :id;";
        $prepareQQcheck = $conn->prepare($QQcheck);
        $prepareQQcheck->bindParam(":id", $_GET['id']);
        $prepareQQcheck->execute();
        $t = $prepareQQcheck->fetch();
        
        if(!empty($t))
        {
            $status = $t['order_status'];
        }
        
        if($_GET['value'] == "up")
        {
            $status += 1;
        }
        
        else if($_GET['value'] == "down")
        {
            $status = $status - 1;
        }
        
        $q = "UPDATE myorder SET order_status = :status  WHERE id_myorder = :id;";
        $stm = $conn->prepare($q);
        $stm->bindParam(":status", $status);
        $stm->bindParam(":id", $_GET['id']);
        if($stm->execute())
        {
            header("Location: index.php?url=orders");           
        }
    }

    // ADD PAGES
    if(isset($_GET['action']) && $_GET['action'] == "addpage" && isset($_SESSION['isLogged']) && isset($_POST['id']) && isset($_POST['title']))
    {
        // walidacja
        $checkLogin = "SELECT id_mypage FROM mypage WHERE id_mypage = :id;";
        $prepareLogin = $conn->prepare($checkLogin);
        $prepareLogin -> bindParam(":id", $_POST['id']);

        if($prepareLogin->execute())
        {
            $toCount = $prepareLogin->fetchAll();

            if(count($toCount) > 0)
            {
                
                echo "<p>Podstrona o danym ID już istnieje!</p>";
                header("Location: index.php?url=pages&value=add-page-error");
            }
            
            else
            {
          
                $q = "INSERT INTO mypage VALUES(NULL, :id, :title)";
                $stm = $conn->prepare($q);
                $stm->bindParam(":title", $_POST['title']);
                $stm->bindParam(":id", $_POST['id']);
                
                if($stm->execute())
                {
                    header("Location: index.php?url=pages");           
                }
            }
        }
    }

    // DELETE PAGES
    if(isset($_GET['action']) && $_GET['action'] == "deletepage" && isset($_SESSION['isLogged']) && isset($_GET['id']))
    {
        
                $q = "DELETE FROM mypage WHERE id_mypage = :id";
                $stm = $conn->prepare($q);
                $stm->bindParam(":id", $_GET['id']);
                
                if($stm->execute())
                {
                    header("Location: index.php?url=pages");           
                }
            
                else
                {
                    echo "<p>Niepowodzenie podczas usuwania strony</p>";
                }
            
    }
    //CODING TRAIN DOSŁOWNIE 
 ?>