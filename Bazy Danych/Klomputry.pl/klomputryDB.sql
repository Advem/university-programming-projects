CREATE DATABASE klomputry;
USE klomputry;

CREATE TABLE myuser
(
	id_myuser INT NOT NULL UNIQUE PRIMARY KEY AUTO_INCREMENT,
	login VARCHAR(20) NOT NULL UNIQUE,
    password VARCHAR(64) NOT NULL,
    firstname VARCHAR(20) NULL, 
    surname VARCHAR(30) NULL,
    email VARCHAR(64) NOT NULL,
    address VARCHAR(64) NULL,
    city VARCHAR(40) NULL, 
    birthdate DATE NULL, 
    age DECIMAL(2),
    phone DECIMAL(9) NULL,
    joineddate DATE NULL,
    role INT NOT NULL
);

INSERT INTO myuser VALUES(NULL, 'filip123', sha2('filip123',256), 'Filip', 'Cyglicki', 'bkjvk@gmail.com', 'Kasztanowa 12', 'Wonna', DATE('1996-03-28'), 22, 123456789, curdate(), 0);
INSERT INTO myuser VALUES(NULL, 'admin', sha2('admin',256), 'Adam', 'Drabik', 'advem@vp.pl', 'Mickiewicza 6', 'Gdansk', DATE('1997-02-01'), 21, 533164479, curdate(), 1);
SELECT * FROM myuser;
DELETE FROM myuser WHERE id_myuser = 1;
DELETE FROM myuser WHERE surname = 'Cyglicki';
DROP TABLE myuser;

DROP MIC;