<?php

    session_start();
    $servername = "localhost";
    $username = "root";
    $password = "";

    try
    {
        $conn = new PDO("mysql:host=$servername;dbname=klomputry", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
        // $nazwa - zmienna w php
        // $get[nazwa] - zmienna z url
        // $post[nazwa] - pole z formularza
        // var nazwa - zmienna javascript
        
    
        // walidacja
        $checkLogin = "SELECT login FROM myuser WHERE login = :userlogin;";
        $prepareLogin = $conn->prepare($checkLogin);
        $prepareLogin -> bindParam(":userlogin", $_POST['userlogin']);
        
        $validation = array(false, false, false, false, false);
        $_SESSION['validate'] = $validation;
        
        if($prepareLogin->execute())
        {
            $toCount = $prepareLogin->fetchAll(); // przechwytuje wszystkie dane z bany danych
            
            if(strlen($_POST['userlogin']) < 5)
            {
                $_SESSION['validate'][0] = false;
                header("Location: index.php?url=signup&value=error-signup-login2");
//                echo "login jest zyt krotki<br>";
            }
            
            else
            {
                $_SESSION['validate'][0] = true;
            } 
            
            if(count($toCount) > 0)
            {
                $_SESSION['validate'][1] = false;
                header("Location: index.php?url=signup&value=error-signup-login1");
//                echo "ten login juz istnieje<br>";
            }
            
            else
            {
                $_SESSION['validate'][1] = true;
            }    
        }
        
        else 
        {
            echo "error przy exec";
        }
        
        if(strlen($_POST['userpassword']) < 5)
        {
            $_SESSION['validate'][2] = false;
            header("Location: index.php?url=signup&value=error-signup-password2");
            echo "haslo jest zbyt krotkie<br>";
        }
        
        else 
        {
            $_SESSION['validate'][2] = true;
        }
        
        if($_POST['userpassword'] != $_POST['userpasswordconfirm'])
        {
            $_SESSION['validate'][3] = false;
            header("Location: index.php?url=signup&value=error-signup-password1");
            echo "hasla musza byc takie same<br>";
        }
        
        else 
        {
            $_SESSION['validate'][3] = true;
        }
        
        
        $checkEmail = "SELECT email FROM myuser WHERE email = :useremail;";
        $prepareEmail = $conn->prepare($checkEmail);
        $prepareEmail->bindParam(":useremail", $_POST['useremail']);
        
        if($prepareEmail -> execute())
        {
            $toCount = $prepareEmail->fetchAll(); // przechwytuje wszystkie dane z bany danych
            
            if(count($toCount) > 0)
            {
                $_SESSION['validate'][4] = false;
                header("Location: index.php?url=signup&value=error-signup-email");
                echo "ten email juz istnieje<br>";
            }
            
            else
            {
                $_SESSION['validate'][4] = true;
            }    
        }
        
     if($_SESSION['validate'][0] && $_SESSION['validate'][1] && $_SESSION['validate'][2] && $_SESSION['validate'][3] && $_SESSION['validate'][4])
     {

         $q = "INSERT INTO myuser VALUES(NULL, :userlogin, sha2(:userpassword, 256), NULL, NULL, :useremail, NULL, NULL, NULL, NULL, curdate(), 0);";
     
        $stm = $conn->prepare($q);
        $stm->bindParam(":userlogin", $_POST['userlogin']);
        $stm->bindParam(":userpassword", $_POST['userpassword']);
        $stm->bindParam(":useremail", $_POST['useremail']);
        
         if($stm->execute())
        {
                $_SESSION['isLogged'] = true;
                $_SESSION['nick'] = $_POST['userlogin'];
                header("Location: index.php");
        }
        
        else
        {
            echo "cos sie nie udalo ale nie wiem co jeszcze";
        }
    }
    }

     catch(PDOException $e)
    {
        echo "Connection failed: " . $e->getMessage();    
    }

?>