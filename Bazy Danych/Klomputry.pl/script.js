function active_tab(id)
{
    document.getElementById(id).style.backgroundColor = "rgba(44, 44, 44, 0.2)";
    document.getElementById(id).style.backgroundSize = "200px 25px";
    document.getElementById(id).style.fontSize = "15px";
    document.getElementById(id).style.fontWeight = "normal";
    document.getElementById(id).style.borderBottom = "thick ridge red";
    document.getElementById(id).style.paddingTop = "5px";
}

function error_login() 
{
        document.getElementById("formlogin").style.backgroundColor = "rgba(173, 33, 33, 0.08)";
        document.getElementById("formpassword").style.backgroundColor = "rgba(173, 33, 33, 0.08)";
        document.getElementById("formlogin").style.borderColor = "rgb(253, 1, 1)";
        document.getElementById("formpassword").style.borderColor = "rgb(253, 1, 1)";
        document.getElementById("error-login-message").innerHTML = "Błędny login lub hasło!";
}

function error_signup_email()
{
    document.getElementById("formemail").style.backgroundColor = "rgba(173, 33, 33, 0.08)";
    document.getElementById("formemail").style.borderColor = "rgb(253, 1, 1)";
    document.getElementById("error-signup-email-message").innerHTML = "Podany email jest już zajęty!";
}

function error_signup_login1()
{
    document.getElementById("formlogin").style.backgroundColor = "rgba(173, 33, 33, 0.08)";
    document.getElementById("formlogin").style.borderColor = "rgb(253, 1, 1)";
    document.getElementById("error-signup-login1-message").innerHTML = "Podany login jest już zajęty!";
}

function error_signup_login2()
{
    document.getElementById("formlogin").style.backgroundColor = "rgba(173, 33, 33, 0.08)";
    document.getElementById("formlogin").style.borderColor = "rgb(253, 1, 1)";
    document.getElementById("error-signup-login2-message").innerHTML = "Login musi posiadać conajmniej 5 znaków!";
}

function error_signup_password1()
{
    document.getElementById("formpassword").style.backgroundColor = "rgba(173, 33, 33, 0.08)";
    document.getElementById("formpasswordconfirm").style.backgroundColor = "rgba(173, 33, 33, 0.08)";
    document.getElementById("formpassword").style.borderColor = "rgb(253, 1, 1)";
    document.getElementById("formpasswordconfirm").style.borderColor = "rgb(253, 1, 1)";
    document.getElementById("error-signup-password1-message").innerHTML = "Obywa hasła muszą być jednakowe!";
}

function error_signup_password2()
{
    document.getElementById("formpassword").style.backgroundColor = "rgba(173, 33, 33, 0.08)";
    document.getElementById("formpassword").style.borderColor = "rgb(253, 1, 1)";
    document.getElementById("error-signup-password2-message").innerHTML = "Hasło musi posiadać conajmniej 5 znaków!";
}

function add_product_correct()
{
    document.getElementById("add-product-correct-message").innerHTML = "Pomyślnie dodano produkt do bazy danych";
}

function add_page_error()
{
    document.getElementById("add-page-error").innerHTML = "Ta podstrona już istnieje!";
}

function open_url(link)
{
	var full_link = "index.php?url=" + link;
	window.location.href = full_link;
}

function toggle_visibility(id, to_hide, to_show)
{

    var id2 = id + '-' + to_show;
    id = id + '-' + to_hide;
    
    document.getElementById(id).style.visibility = "visible"; // tu powinnno byc hidden
    document.getElementById(id2).style.visibility = "visible";

}

function change_data(id)
{
    var xhttp = new XMLHttpRequest();
    var value = document.getElementById(id + "-input").value; 
    
    xhttp.onreadystatechange = function()
    {
        if (this.readyState == 4 && this.status == 200) 
        {
            toggle_visibility(id, "change", "data");
            document.getElementById(id + "-show-data").innerHTML = this.responseText;
        }
    };
        xhttp.open("GET", "function.php?q=change&what="+id+"&value="+value, true);
        xhttp.send();
}

function change_product_data(id)
{
    var xhttp = new XMLHttpRequest();
    var value = document.getElementById(id + "-input").value; 
    var name = document.getElementById("searchproduct").value; //usunac
    
    xhttp.onreadystatechange = function()
    {
        if (this.readyState == 4 && this.status == 200) 
        {
            toggle_visibility(id, "change", "data");
            document.getElementById(id + "-show-data").innerHTML = this.responseText;
        }
    };
        xhttp.open("GET", "function.php?q=editproduct&what="+id+"&value="+value+"&name="+name, true);
        xhttp.send();
}

function find_user()
{
    var xhttp = new XMLHttpRequest();
    var nick = document.getElementById("searchuser").value;
    
     xhttp.onreadystatechange = function()
    {
        if (this.readyState == 4 && this.status == 200) 
        {
            document.getElementById("profile-info").innerHTML = this.responseText;
        }
    };
    
    xhttp.open("GET", "function.php?q=finduser&value="+nick);
    xhttp.send();
 
}

function delete_user()
{
   var xhttp = new XMLHttpRequest();
    var nick = document.getElementById("searchuser").value;
    
     xhttp.onreadystatechange = function()
    {
        if (this.readyState == 4 && this.status == 200) 
        {
            document.getElementById("profile-info").innerHTML = this.responseText;  
        }
    };
    
    if(confirm("Czy na pewno chcesz usunąć użytkownika "+nick+" ?"))
    {
        xhttp.open("GET", "function.php?q=deleteuser&value="+nick);
        xhttp.send(); 
    }
}

function delete_product()
{
    var xhttp = new XMLHttpRequest();
    var name = document.getElementById("searchproduct").value;
    
     xhttp.onreadystatechange = function()
    {
        if (this.readyState == 4 && this.status == 200) 
        {
            document.getElementById("result").innerHTML = this.responseText;
            
        }
    };
    
    if(confirm("Czy na pewno chcesz usunąć product "+name+" ?"))
    {
        xhttp.open("GET", "function.php?q=deleteproduct&value="+name);
        xhttp.send(); 
    }
}

function toscik(title, message)
{
    iziToast.show
    ({
    theme: 'dark',
    color: 'rgba(44, 44, 44, 0.8)',
    icon: 'icon-person',
    title: false,
    titleSize: '12',
    message: message,
    messageColor: 'white',
    messageSize: '12',
    position: 'bottomCenter', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter
    //progressBarColor: 'rgb(0, 255, 184)',
    progressBar: false,
    animateInside: false,
    drag: false,
    displayMode: '0',
    transitionIn: 'bounceDown',
    close: false,
    timeout: 2000,
    imageWidth: '1',
    image: null,
    icon: null,
    overlay: false,
    overlayColor: 'rgba(255, 196, 0, 0.6)',
    
});
}

function cartadd(product)
{
    let url = "function.php?action=add&id=" + product;
    $.get(url, function(data) 
    {

       toscik("","Dodano produkt do koszyka");
        
    });
}

function cartadd_error()
{
    toscik("","Zaloguj się, aby dokonać zakupu!"); 
}

function cartdelete(product)
{
    let url = "function.php?action=delete&id=" + product;
    $.get(url, function(data) 
    {
        toscik("","Usunieto produkt z koszyka");
    });
}

function cartorder(total)
{       
    let url = "function.php?action=order&total=" + total;
    $.get(url, function(data) 
    {
      //toscik("","Usunięto produkt z koszyka!"); /
        iziToast.show
    ({
    theme: 'dark',
    color: 'rgba(44, 44, 44, 0.8)',
    icon: 'icon-person',
    title: false,
    titleSize: '12',
    message: 'Zamówienie zostało przesłane. Dziękujęmy za zakup!',
    messageColor: 'white',
    messageSize: '12',
    position: 'center', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter
    //progressBarColor: 'rgb(0, 255, 184)',
    progressBar: false,
    animateInside: false,
    drag: false,
    displayMode: 'once',
    transitionIn: 'bounceDown',
    close: true,
    timeout: 5000,
    imageWidth: '1',
    image: null,
    icon: null,
    overlay: true,
    overlayColor: 'rgba(0, 0, 0, 0.75)',
    onClosing: function(){
        window.open("index.php?url=cart","_self");
    }

    
});
    });
}
