CREATE DATABASE klomputry;
USE klomputry;

CREATE TABLE myuser
(
	id_myuser INT NOT NULL UNIQUE PRIMARY KEY AUTO_INCREMENT,
	login VARCHAR(20) NOT NULL UNIQUE,
    password VARCHAR(64) NOT NULL,
    firstname VARCHAR(20) NULL, 
    surname VARCHAR(30) NULL,
    email VARCHAR(64) NOT NULL,
    address VARCHAR(64) NULL,
    city VARCHAR(40) NULL, 
    birthdate DATE NULL, 
    phone DECIMAL(9) NULL,
    joineddate DATE NULL,
    role INT NOT NULL
);

CREATE TABLE myproduct
(
	id_myproduct INT NOT NULL UNIQUE PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(60) NOT NULL,
    description VARCHAR(150) NULL,
    price VARCHAR(20) NOT NULL,
    category INT NULL,
    image VARCHAR (150) NULL
);

INSERT INTO myuser VALUES(NULL, 'filip123', sha2('filip123',256), 'Filip', 'Cyglicki', 'bkjvk@gmail.com', 'Kasztanowa 12', 'Wonna', DATE('1996-03-28'), 123456789, curdate(), 0);
INSERT INTO myuser VALUES(NULL, 'admin', sha2('admin',256), 'Adam', 'Drabik', 'advem@vp.pl', 'Mickiewicza 6', 'Gdansk', DATE('1997-02-01'), 53164479, curdate(), 1);
INSERT INTO myuser VALUES(NULL, 'user', sha2('haslo',256), 'Adam', 'Drabik', 'advem@vp.pl', 'Mickiewicza 6', 'Gdansk', DATE('1997-02-01'), 53164479, curdate(), 0);
SELECT * FROM myuser;
DELETE FROM myuser WHERE surname = 'Cyglicki';
DROP TABLE myuser;

INSERT INTO myproduct VALUES(NULL, 'Klomputry numero uno', 'Pierwszy produkt stworzony dla klomputry.pl', 69.99, 0, 'https://cdn0.iconfinder.com/data/icons/basic-uses-symbol-vol-2/100/Help_Need_Suggestion_Question_Unknown-512.png');
INSERT INTO myproduct VALUES(NULL, 'Klomputry numero trio', 'Trzeci produkt stworzony dla klomputry.pl', 3679.99, 1, 'https://cdn0.iconfinder.com/data/icons/basic-uses-symbol-vol-2/100/Help_Need_Suggestion_Question_Unknown-512.png');
INSERT INTO myproduct VALUES(NULL, 'Klomputry bardzo fajnie duzo tekstu spoko nazwa', 'Jakis o wiele dluzszy tekst ktory zajmuje znacznie, ale to znacznie wiecej mniejsca niz poprzedni, ale bez przesady', 30878.00, 1, 'https://cdn0.iconfinder.com/data/icons/basic-uses-symbol-vol-2/100/Help_Need_Suggestion_Question_Unknown-512.png');
INSERT INTO myproduct VALUES(NULL, 'Turbopszczelarz.pl', 'Tania strona WWW na sell dla chorych fanatykow miodu', 00.00, -1, 'tp.png');
INSERT INTO myproduct VALUES(NULL, 'test', 'Opis testu', 100.00, 0, 'tp.png');

DELETE FROM myproduct WHERE id_myproduct = 49;
DELETE FROM myproduct WHERE id_myproduct > 32;
SELECT * FROM myproduct;
DROP TABLE myproduct;