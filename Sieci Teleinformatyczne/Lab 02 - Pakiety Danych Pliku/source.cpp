﻿#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <iomanip>
#include <algorithm>
#include <time.h>

using namespace std;

double dataSize;
double total = 0;
void SetDataSize(double &dataSize, int checkDataSize ) { dataSize = checkDataSize; }

struct Packet
{
	int ID, SZ;
	Packet(int ID, int size):
			ID(ID), SZ(SZ) {};
};

void PrintPacketsData(Packet *pack, vector<char*> vecPack, double dataSize)
{
	double percentage = 100.0*(pack->SZ)/dataSize;
	total += percentage;
	cout << "Package: " << pack->ID << " | Size: " << pack->SZ << " (" << setprecision(2) << percentage << "%) | Loaded: " << total << "% | Data: ";
	cout.write( (char*)(vecPack.back() + sizeof(Packet) ), pack->SZ);
	cout << endl;
}

vector<char> ReadFileData(string fileName) 
{
	cout << endl << "# Reading Data from file: " << fileName << endl << endl;

	fstream file;
	file.open(fileName, ios::in | ios::binary);
	if (!file.good()) 	cout << "Cannot open the file: " << fileName << endl;
	
	vector<char> data;
	char c;
	while (file.get(c)) data.push_back(c);
	return data;
}

vector<char*> ReadPacketsData(string fileName)
{
	cout << endl << "#  Reading Packets from file: " << fileName << endl << endl;

	fstream file;
	file.open(fileName, ios::in | ios::binary);
	if (!file.good()) 	cout << "Cannot open the file: " << fileName << endl;

	vector<char*> vecPack;
	int current_id;
	int current_size;
	char* data;
	int i = 0;

	while (file >> current_id && file >> current_size)
	{
		data = new char[current_size];
		if (file.peek() == ' ') file.ignore(1);
		file.read(data, current_size);

		vecPack.push_back(new char[sizeof(Packet) + current_size * sizeof(char)]);
		Packet* pack = (Packet*)vecPack.back();
		pack->ID = current_id;
		pack->SZ = current_size;
		char* packetaData = (char*)(vecPack.back() + sizeof(Packet));

		strncpy(packetaData,  data, current_size );

		PrintPacketsData(pack, vecPack, dataSize);
	}
	
	cout << endl << "#  Not fixed packets data: " << endl << endl;

		for (auto x : vecPack)
		{
			Packet* pack = (Packet*) x;
			cout.write((char*)(x + sizeof(Packet)), pack->SZ);
		}
	cout << endl;
	file.close();
	return vecPack;
}

void WriteFileData(string fileName, vector<char*> &data)
{
	cout << endl << "#  Writing Data to file: " << fileName << endl;

	fstream file;
	file.open(fileName, ios::out | ios::binary);
	if (!file.good()) 	cout << "Cannot open the file: " << fileName << endl;

	for (int i = 0; i < data.size(); i++)
	{
		Packet* pack = (Packet*)data[i];
		char* packetaData = data[i] + sizeof(Packet);

		file.write(packetaData, pack->SZ);
	}
	file.close();
}

void WritePacketsData(string fileName, vector<char*> &data)
{
	cout << endl << "#  Writing Packets to file: " << fileName << endl;

	fstream file;
	file.open(fileName, ios::out | ios::binary);
	if (!file.good()) 	cout << "Cannot open the file: " << fileName << endl;
	
	for (int i = 0; i < data.size(); i++)
	{
		Packet* pack = (Packet*) data[i];
		char* packetaData = data[i] + sizeof(Packet);
		file << pack->ID << " " << pack->SZ << " ";
		file.write(packetaData, pack->SZ);
		file << "\n";
	}

	file.close();
}

void EraseVector(vector<char*> vec)
{
	for (int i = vec.size() - 1; i >= 0; i--) delete[] vec[i];
}

vector<char*> FixData(vector<char*> vecPack)
{
	vector<char*> vecFixed;
	cout << endl << "#  Fixing packets data: " << endl << endl;

	for (int i = 0; i < vecPack.size(); i++)
		for (auto x : vecPack)
		{
			Packet* pack = (Packet*)x;
			if (pack->ID == i)
			{
				vecFixed.push_back(x);
				cout.write((char*)(x + sizeof(Packet)), pack->SZ);
				break;
			}
		}
	cout << endl;
	return vecFixed;
}

int main(int argc, char *argv[])
{
	int set_size = 24; /// set package size
	int current_id = 0;
	int current_size;
	bool fixPacketsOrder = true; // czy poskładać pakiety ?
	string inputFileName = "input.txt"; // input.txt 
	string outputFileName = "output.txt"; //output.txt
	string outputFileNameFix = "output_sorted.txt"; //output_sorted.txt
	vector<char> data = ReadFileData(inputFileName); 
	vector<char*> vecPack;

	/// Drukuje informacje o pliku
	SetDataSize(dataSize, data.size());
	cout << "File size: " << dataSize << " (" << dataSize*8 << "b)" << " | " 
			 << "Packages: " << ceil(dataSize / set_size) << endl << endl;

	/// Rozbicie danych na pakiety
	for (int i = 0; i < data.size(); i += set_size)
	{
		current_size = 0;
		if (data.size() - 1 >= set_size + i) current_size = set_size;
		else current_size = data.size() % i;
		
		vecPack.push_back(new char[sizeof(Packet) + current_size * sizeof(char)]);
		Packet* pack = (Packet*) vecPack.back();
		pack->ID = current_id;
		pack->SZ = current_size;
		current_id++;
		char* packetaData = vecPack.back() + sizeof(Packet);
		
		for (int j = i; j < i + current_size; j++)
		{ *packetaData = data[j]; packetaData++; }
	
		PrintPacketsData(pack, vecPack, dataSize);
	}

	/// PRZEMIESZANIE PAKIETÓW
	total = 0;
	random_shuffle(vecPack.begin(), vecPack.end());
	WritePacketsData(outputFileName, vecPack);
	EraseVector(vecPack);
	vecPack = ReadPacketsData(outputFileName);

	/// SKŁADANIE PAKIETÓW
	vector<char*> vecFixed = FixData(vecPack);
	if (fixPacketsOrder) WriteFileData(outputFileNameFix, vecFixed);
	else WriteFileData(outputFileNameFix, vecPack);
	
	EraseVector(vecPack);

	cout << endl;
	system("PAUSE");
	return 0;
}