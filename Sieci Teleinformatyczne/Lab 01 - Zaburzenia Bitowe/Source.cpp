﻿#include <iostream>
#include <iterator>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <algorithm>
#include <bitset>
#include <ctime>

using namespace std;

void Binary_conventer(vector<char> vec_ch, vector<bool> &vec_b)
{
	vec_b.erase(vec_b.begin(), vec_b.end()); // czysci caly wektor bo sie nadpisywal po transmisji
	bitset<8> mybit;
	for (int i = 0; i < vec_ch.size(); i++)
	{
		mybit = vec_ch[i];
		mybit.to_string();
		for (int j = 7; j >= 0; j--) vec_b.push_back(mybit[j]);
	}
}

vector<char> Read_file_char(string file_name)
{
	ifstream myfile(file_name, ios::binary);
	vector<char> data((istreambuf_iterator<char>(myfile)), istreambuf_iterator<char>());
	cout << "//data has been read from " << file_name << " file." << endl << endl;
	cout << "data: " << endl;
	copy(data.begin(), data.end(), ostream_iterator<char>(cout, ""));
	cout << endl << endl;
	return data;
}

void Save_file_char(string file_name, vector<bool> vec)
{
	string data;
	string output;
	for (int i = 0; i < vec.size(); i++)
	{
		if (vec[i] == 1) data.push_back('1');
		else data.push_back('0');
	}

	// dopisuje zera przy bicie parzystoscie przed zapisem do pliku
	//for (int i = 0; i < 7; i++) data.insert(data.end() - 1, '0');

	stringstream sstream(data);

	cout << "string:" << endl;
	for (int i = 0; i < data.size(); i++)
	{
		cout << data[i];
		if ((i + 1) % 8 == 0) cout << " ";
	}
	cout << endl << endl;

	while (sstream.good())
	{
		bitset<8> bits;
		sstream >> bits;
		char c = char(bits.to_ulong());
		output += c;
	}

	cout << "output: " << endl << output << endl;

	ofstream output_file(file_name, ios::binary);
	ostream_iterator<char> output_iterator(output_file, "");
	copy(output.begin(), output.end(), output_iterator);

	cout << endl << "//data has been saved to " << file_name << " file." << endl << endl;
}

void Bit_parzystosci(vector<bool> &vec, bool print_cout)
{
	int count_1s = 0;
	for (auto x : vec) if (x) count_1s++;
	vec.push_back(count_1s % 2);
	if (print_cout) cout << "//counted(1s): " << count_1s << " = bit parzystosci: " << count_1s % 2 << endl << endl;
}

void Suma_modulo(int a, vector<bool> &vec, int n)
{
	int x, result, a2;
	a2 = a;
	result = 1;
	a = a % n;
	x = a;
	for (int i = vec.size() - 1; i >= 0; i--)
	{
		if (vec[i])
		{
			result = result * x;
			result = result % n;
		}
		x = x * x;
		x = x % n;
	}
	cout << "//counted: " << a2 << "^" << "bit" << " mod " << n << " = " << result << endl << endl;

	bitset<8> mybit(result);
	mybit.to_string();
	for (int j = mybit.size() - 1; j >= 0; j--) vec.push_back(mybit[j]);
}

void Suma_modulo2(vector<char> vec, vector<bool> &vec2, int val)
{
	int i = 0, sum = 0;
	while (i < vec.size())
	{ sum += vec[i]; i++; }
	cout << "//result = " << sum << " % " << val << " = " << sum % val << endl;
	sum = sum % val;
	
	bitset<8> mybit(sum);
	mybit.to_string();
	for (int j = mybit.size() - 1; j >= 0; j--) vec2.push_back(mybit[j]);
}

void Distortion(vector<bool> &vec, double percentage, bool include_repeats)
{
	vector<bool> repetition; // wektor zapisujacy ktore miejsce bylo juz zaburzone
	int changes = vec.size() * percentage / 100;
	srand(time(NULL));
	//vec[rand() % vec.size()];

	for (int i = 0; i < changes; i++)
	{
		bool new_value = false;
		int temp = rand() % vec.size();
		if (include_repeats)
		{
			while (!new_value)
			{
				new_value = true; // czy znalezlismy nowa niepowtarzalna wartosc
				for (auto x : repetition)
					if (x == temp)
					{
						temp = rand() % vec.size();
						new_value = false;
						break;
					}
			}
			repetition.push_back(temp);
		}
		vec[temp] = !vec[temp];
	}

	cout << "//data has been distorted (" << percentage << "%) ";
	if (include_repeats) cout << "with repetition";
	else cout << "without repetition";
	cout << endl << endl;
}

void Add_zeros(vector<bool> &vec, int zeros, int before_end)
{
	for (int i = 0; i < zeros; i++)
		vec.insert(vec.end() - before_end, 0);
	cout << "//added " << zeros << " zeros to bit." << endl << endl;
}

void Add_zeros_advanced(vector<bool> &vec, int crc_n)
{
	int zeros;
	int byte_length = 8;

	while (byte_length < crc_n) byte_length += 8;
	zeros = byte_length - crc_n;

	if (crc_n != zeros)
	for (int i = 0; i < zeros; i++)
		vec.insert(vec.end() - crc_n, 0);
	cout << "//added " << zeros << " zeros to bit." << endl << endl;

}

void Delete_zeros_advanced(vector<bool> &vec, int crc_n)
{
	int zeros;
	int byte_length = 8;

	while (byte_length < crc_n) byte_length += 8;
	zeros = byte_length - crc_n;

	if (crc_n != zeros)
		for (int i = 0; i < zeros; i++)
			vec.erase(vec.end() - 1 - crc_n);
	
	cout << "//removed " << zeros << " zeros from bit." << endl << endl;
}

void Delete_zeros(vector<bool> &vec, int zeros)
{
	vec.erase(vec.end() - 1 - zeros, vec.end() - 1);
}

void Delete_last_bit(vector<bool> &vec, int numbers)
{
	vec.back();
	for (int i = 0; i < numbers; i++) vec.pop_back();
}

void Merge(vector<bool>& v1, vector<bool> v2, int crc_n)
{
	for (int i = crc_n - 1; i >= 0; i--)
		v1.push_back(v2[v2.size() - 1 - i]);
	cout << "// added CRC(" << crc_n << ") = ";

	for (int i = crc_n - 1; i >= 0; i--)
		cout << v1[v1.size() - 1 - i];

	cout << " to bits." << endl << endl;
}

void Print_info(vector<char> data)
{
	//cout << "txt: ";
	//for (auto n : data) cout << n << " ";

	cout << "ascii: ";
	for (auto n : data) cout << (int)n << " ";
	cout << endl << endl;
}

void Print_vector(vector<bool> vec, int space)
{
	cout << "bit(" << vec.size() << "): ";
	cout << endl;
	for (int i = 0; i < vec.size(); i++)
	{
		cout << vec[i];
		if (space > 0) if ((i + 1) % space == 0) cout << " ";
	}
	cout << endl << endl;
}

void Print_crc_vec(vector<bool> vec, int crc_n)
{
	for (int i = 0; i < vec.size(); i++)
	{
		cout << vec[i];
		if (i == vec.size() - 1 - crc_n) cout << " ";
	} 
	cout << endl;
}

void Print_crc_key(vector<bool> crc_key, int spaces)
{
	if (spaces > 1) for (int i = 0; i < spaces - 1; i++) cout << " ";
	for (auto x : crc_key) cout << x;
	cout << endl << endl;
}

void Generate_crc_key(vector<bool> &crc_key, int crc_n, double crc_value)
{
	crc_key.clear();
	bitset<64> mybit(crc_value);
	mybit.to_string();
	crc_key.push_back(1);
	for (int i = crc_n - 1; i >= 0; i--) crc_key.push_back(mybit[i]);
	// crc_key.clear(); crc_key = {1,0,0,1,1,0,0,1,1}; // DOWOLNY WIELOMIAN ?
	cout << "//genetared " << crc_n + 1 << "bit crc_key for CRC" << crc_n << " from value(" << crc_value << "): ";
	for (auto x : crc_key) cout << x;
	cout << endl << endl;
}

void Xor(vector<bool> &v1, vector<bool> &v2, int spaces)
{
		for (int i = 0; i < v2.size(); i++)
		{
			if (i + spaces < v1.size())
			{
				if (v1[i + spaces] == v2[i]) v1[i + spaces] = 0;
				else v1[i + spaces] = 1;
			}
		}
}

void CRC(vector<bool> &vec, int crc_n, double crc_value, vector<bool> &crc_key)
{
	int spaces = 0; 
	Generate_crc_key(crc_key, crc_n, crc_value);
	Add_zeros(vec, crc_n, 0);
		
	while (spaces + crc_key.size() <= vec.size())
	{
		Print_crc_vec(vec, crc_n);

		while (vec[spaces] == 0) 
		{
			spaces++;
		}
		
		if (spaces + crc_key.size() <= vec.size())
		{
			Xor(vec, crc_key, spaces);
			spaces++;
			//cout << "space = " << spaces << endl;
			Print_crc_key(crc_key, spaces);
		}
	}

	Print_crc_vec(vec, crc_n);
	cout << endl;
}

int main()
{
	int choice = 3; // wybor algorytmu 1/2/3
	int mod_value = 12; // wartosc do sumy modulo
	int crc_n = 64; // n-bitowy algorytm crc
	double crc_value = 1800800975486347548; // wartosc do obliczenia klucza crc (dowolna liczba)
	double percentage = 1; // procent zaburzenia w % nie wartosc 0 - 1
	bool repetition = 0; // zaburzenie z/bez powtorzen

	vector<char> data; // symbole z pliku data
	vector<bool> bitz; // binarne z pliku data
	vector<bool> bitz_save; // binarna kopia bitz przed zaburzeniem
	vector<bool> crc_key; // dzielnik crc
	//vector<bool> bitz_crc = { 1,1,0,1,0,0,1,1,1,0,1,1,1,0 }; // DOWOLNY CIAG ? ODKOMENTUJ

	/// WCZYTANIE PLIKU DO VEC.BOOL
	data = Read_file_char("data.txt");
	Print_info(data);
	Binary_conventer(data, bitz);
	Print_vector(bitz, 8);
	vector<bool> bitz_crc = bitz; // binarne z pliku data do crc // DOWOLNY CIAG ? ZAKOMENTUJ
	vector<bool> bitz_save_crc = bitz; // save -||-

	/// 1. - BIT PARZYSTOSCI
	if (choice == 1)
	{
		/// OBLICZENIE WYNIKU 
		Bit_parzystosci(bitz, 1);
		Print_vector(bitz, 8);

		/// ZAPISANIE PLIKU
		Add_zeros(bitz, 7, 1);
		Print_vector(bitz, 8);
		Save_file_char("output.txt", bitz);

		/// WCZYTANIE PLIKU
		data = Read_file_char("output.txt");
		Print_info(data);
		Binary_conventer(data, bitz);
		Delete_last_bit(bitz, 8);
		Delete_zeros(bitz, 7);
		Print_vector(bitz, 8);
		bitz_save = bitz;

		/// ZABURZENIE DANYCH
		Distortion(bitz, percentage, repetition);
		Print_vector(bitz, 8);
		Save_file_char("output.txt", bitz);

		/// OBLICZENIE WYNIKU
		Delete_last_bit(bitz, 1);
		Bit_parzystosci(bitz, 1);
		cout << "Before distortion - ";
		Print_vector(bitz_save, 8);
		cout << "After distortion  - ";
		Print_vector(bitz, 8);
	}

	/// 2. - SUMA MODULO
	else if (choice == 2)
	{
		/// OBLICZENIE WYNIKU 
		Suma_modulo2(data, bitz, mod_value);
		Print_vector(bitz, 8);

		/// ZAPISANIE PLIKU
		Save_file_char("output.txt", bitz);

		/// WCZYTANIE PLIKU
		data = Read_file_char("output.txt");
		Print_info(data);
		Binary_conventer(data, bitz);
		Delete_last_bit(bitz, 8);
		Print_vector(bitz, 8);
		bitz_save = bitz;

		/// ZABURZENIE DANYCH
		Distortion(bitz, percentage, repetition);
		Print_vector(bitz, 8);
		Save_file_char("output.txt", bitz);

		/// OBLICZENIE WYNIKU
		Delete_last_bit(bitz, 8);
		Suma_modulo2(data, bitz, mod_value);
		cout << "Before distortion - ";
		Print_vector(bitz_save, 8);
		cout << "After distortion  - ";
		Print_vector(bitz, 8);
	}

	/// 3. - CYKLICZNY KOD NADMIAROWY
	else if (choice == 3)
	{
		/// OBLICZENIE WYNIKU
		CRC(bitz_crc, crc_n, crc_value, crc_key);
		Print_crc_vec(bitz_crc, crc_n);
		cout << endl;
		Print_vector(bitz, 8);
		Merge(bitz, bitz_crc, crc_n);
		Print_vector(bitz, 8);

		/// ZAPISANIE PLIKU
		Add_zeros_advanced(bitz, crc_n);
		Print_vector(bitz, 8);
		Save_file_char("output.txt", bitz);

		/// WCZYTANIE PLIKU
		data = Read_file_char("output.txt");
		Print_info(data);
		Binary_conventer(data, bitz);
		Delete_last_bit(bitz, 8); //usuwa niechciane 0 a nie crc
		Print_vector(bitz, 8);
		
		/// ZABURZENIE DANYCH
		Delete_zeros_advanced(bitz, crc_n);
		bitz_save = bitz;
		Print_vector(bitz, 8);
		Distortion(bitz, percentage, repetition);
		Print_vector(bitz, 8);
		Save_file_char("output.txt", bitz);
		bitz_save_crc = bitz;

		/// OBLICZENIE WYNIKU
		bitz_crc.clear();
		bitz_crc = bitz_save_crc;
		CRC(bitz_crc, crc_n, crc_value, crc_key);
		Delete_last_bit(bitz, crc_n);
		Merge(bitz, bitz_crc, crc_n);
		cout << "Before distortion - ";
		Print_crc_vec(bitz_save, crc_n);
		cout << endl;
		cout << "After distortion  - ";
		Print_crc_vec(bitz, crc_n);
		
	}

	else cout << "// wrong algorithm chosen!" << endl;

	cout << endl;
	system("pause");
	return 0;
}