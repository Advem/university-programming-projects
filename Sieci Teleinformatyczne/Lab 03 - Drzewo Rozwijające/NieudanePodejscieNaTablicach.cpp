﻿#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <set>

/// UPDATE: algorytm nie zapamietuje poprzednich iteracji, więc nie jest w stanie powrócić do wcześniejszego wierzchołka gdy waga krawędzi następnej iteracji jest większa od wcześniej niewykorzystanej krawędzi iteracji poprzedniej

using namespace std;

void CreateEdges(int n)
{
	for (int i = 0; i < n; i++)
	{ }
}

void CreateTree(vector<vector<int>>& tree, int no_points, int no_edges)
{
	for (int i = 0; i < no_points; i++)
		for (int j = 0; j < no_points; j++)
			tree[i].push_back(0);
}

void PrintTree(vector<vector<int>> tree, int no_points)
{
	for (int i = 0; i < no_points; i++)
		for (int j = 0; j < no_points; j++)
			if(tree[i][j] != 0) cout << "[" << i << "] [" << j << "]  =  " << tree[i][j] << endl;
}

void SetSameWeights(vector<vector<int>> &tree, int no_points)
{
	for (int i = 0; i < no_points; i++)
		for (int j = 0; j < no_points; j++)
			if (tree[i][j] > 0) tree[j][i] = tree[i][j];
}

void SetWeights(vector<vector<int>> &tree, int no_points)
{
	tree[0][1] = 4;
	tree[0][4] = 1;
	tree[0][5] = 2;
	tree[1][2] = 2;
	tree[1][4] = 2;
	tree[2][3] = 8;
	tree[3][4] = 3;
	tree[3][5] = 6;
	tree[4][5] = 7;
	
	SetSameWeights(tree, no_points);
}

void PrintVec(vector<int> vec)
{
	for (auto x : vec) cout << x << " ";
	cout << endl;
}

int WhereIsTheSmallest(vector<int> vec)
{
	int position = 99;
	for (int i = 0; i < vec.size(); i++)
		if (vec[i] < position) position = i;
	return position;

}

int main()
{
	vector<vector<int>> tree = { {0,0,0,0,0,0}, {0,0,0,0,0,0}, {0,0,0,0,0,0}, {0,0,0,0,0,0}, {0,0,0,0,0,0}, {0,0,0,0,0,0} }; // !!! zrobić do tego funkcje
	int no_points = 6;

	//CreateTree(tree, no_points, no_edges);
	SetWeights(tree, no_points);
	PrintTree(tree, no_points);

	int initial_point = 0; // zacznij od wierzchołka
	int initial_point_prime = initial_point;
	int weight_sum = 0;
	int the_smallest_value = 99; // jakas duza wartosc
	int the_smallest_point = 99; // jakas duza wartosc

	vector<int> save_val;
	vector<int> save_point;
	set<int> sett;

	for (int i = 0; i < 10; i++)
	{
		// analizuje wszystkie krawedzi danego wierzcholka w poszukiwaniu najmniejszej wagi
		for (int i = initial_point; i <= initial_point; i++)
		{
			// porownuje wagi krawedzi sasiednich wierzcholkow
			for (int j = 0; j < no_points; j++)
			{
				if (tree[i][j] > 0) cout << "Checking [" << i << "] [" << j << "] = " << tree[i][j] << "  . . .  ";

				// zapamietuje ktory sasiedni wierzcholek laczy sie po najmniejszej wadze
				if (tree[i][j] > 0 and tree[i][j] < the_smallest_value)
				{
					the_smallest_value = tree[i][j];
					the_smallest_point = j;
					// zapisuje do wektora wszystkie wagi
					save_val.push_back(tree[i][j]);
					// zapisuje do wektora wszystkie wiercholki
					save_point.push_back(j);
				}

				if (tree[i][j] > 0) cout << "\t the smallest: [" << initial_point << "] [" << the_smallest_point << "] = " << the_smallest_value << endl;
			}
		}
		// zeruje uzyte krawedzi
		tree[initial_point][the_smallest_point] = 0;
		tree[the_smallest_point][initial_point] = 0;

		// dodaje wagi do kosztow
		weight_sum += the_smallest_value;
	
		// sortowanie i usuniecie najmniejszej wagi z wektora
		cout << endl << " weights: ";
		PrintVec(save_val);
		cout << "points: ";
		PrintVec(save_point);
		//sort(save_val.begin(), save_val.end());
		//save_val.erase(remove(save_val.begin(), save_val.end(), the_smallest_value), save_val.end());
		int position = WhereIsTheSmallest(save_val);
		//save_val.erase(save_val.begin() + position);
		//save_point.erase(save_point.begin() + position);
		cout << endl << "weights: ";
		PrintVec(save_val);
		cout << "points: ";
		PrintVec(save_point);
		//PrintVec(save_val);

		// przesuniecie iteracji do nastepnego wiercholka 
		the_smallest_value = 99;
		initial_point = the_smallest_point;
		cout << endl;

		if (initial_point_prime == the_smallest_point) break;
	}

	cout << "Weights sum: " << weight_sum << endl << endl;
	system("PAUSE");
	return EXIT_SUCCESS;
}