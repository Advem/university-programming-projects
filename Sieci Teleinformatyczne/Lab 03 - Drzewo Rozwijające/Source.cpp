﻿#include <iostream>
#include <vector>
#include <algorithm>
#include <chrono>

using namespace std;

/// ALGORYTM  KRUSKALA

typedef  pair<int, int> iPair;

struct Graph
{
	int V, E;
	vector< pair<int, iPair> > edges;

	Graph(int V, int E)
	{
		this->V = V;
		this->E = E;
	}

	void addEdge(int u, int v, int w)
	{
		edges.push_back({ w, {u, v} });
	}
	int kruskalMST();
};

struct DisjointSets
{
	int *parent, *rnk;
	int n;

	DisjointSets(int n)
	{
		this->n = n;
		parent = new int[n + 1];
		rnk = new int[n + 1];

		for (int i = 0; i <= n; i++)
		{
			rnk[i] = 0;
			parent[i] = i;
		}
	}

	int find(int u)
	{
		if (u != parent[u])
			parent[u] = find(parent[u]);
		return parent[u];
	}

	void merge(int x, int y)
	{
		x = find(x), y = find(y);

		if (rnk[x] > rnk[y])
			parent[y] = x;
		else // If rnk[x] <= rnk[y] 
			parent[x] = y;

		if (rnk[x] == rnk[y])
			rnk[y]++;
	}
};

int Graph::kruskalMST()
{
	int mst_wt = 0; // zacznij od wierzchołka
	sort(edges.begin(), edges.end());
	DisjointSets ds(V);

	vector< pair<int, iPair> >::iterator it;
	for (it = edges.begin(); it != edges.end(); it++)
	{
		int u = it->second.first;
		int v = it->second.second;
		int set_u = ds.find(u);
		int set_v = ds.find(v);

		if (set_u != set_v)
		{
			cout << u << " - " << v << endl;
			mst_wt += it->first;
			ds.merge(set_u, set_v);
		}
	}

	return mst_wt;
}

/// ALGORYTM PRIMA ////////////////////////

#define VX 5 // liczba krawędzi
int cost = 0;
int minKey(int key[], bool mstSet[])
{
	int min = INT_MAX, min_index;

	for (int v = 0; v < VX; v++)
		if (mstSet[v] == false && key[v] < min)
			min = key[v], min_index = v;

	return min_index;
}

void printMST(int parent[], int n, int graph[VX][VX])
{
	printf("Edge \tWeight\n");
	for (int i = 1; i < VX; i++)
	{
		printf("%d - %d \t%d \n", parent[i], i, graph[i][parent[i]]);
		cost += graph[i][parent[i]];
	}
}

void primMST(int graph[VX][VX])
{
	int parent[VX];
	int key[VX];
	bool mstSet[VX];

	for (int i = 0; i < VX; i++)
		key[i] = INT_MAX, mstSet[i] = false;
 
	key[0] = 0;
	parent[0] = -1;  

	for (int count = 0; count < VX - 1; count++)
	{
		int u = minKey(key, mstSet);

		mstSet[u] = true;

		for (int v = 0; v < VX; v++)
			if (graph[u][v] && mstSet[v] == false && graph[u][v] < key[v])
				parent[v] = u, key[v] = graph[u][v];
	}
	printMST(parent, VX, graph);
}

int main()
{
	int pick;
	cout << "1. Kruskala" << endl << "2. Prima" << endl;
	cout << "Wybierz algorytm:";
	cin >> pick;

	if (pick == 1)
	{
		// Start timer
		auto start = chrono::high_resolution_clock::now();
		int V = 5, E = 8; // liczba wierchołków , liczba krawędzi
		Graph g(V, E);
		// (lewy wierzchołek, prawy wierchołek, waga)
		g.addEdge(0, 1, 1);
		g.addEdge(0, 2, 2);
		g.addEdge(0, 3, 2);
		g.addEdge(0, 4, 6);
		g.addEdge(1, 3, 4);
		g.addEdge(1, 4, 5);
		g.addEdge(2, 3, 3);
		g.addEdge(3, 4, 3);
	

		cout << "Edges of MST are \n";
		int mst_wt = g.kruskalMST();

		cout << "\nWeight of MST is " << mst_wt << endl;
		// Stop timer
		auto finish = chrono::high_resolution_clock::now();
		chrono::duration<double> elapsed = finish - start;
		cout << "Elapsed time: " << elapsed.count() << " s" << endl;
	}

	if (pick == 2)
	{
		// Start timer
		auto start = chrono::high_resolution_clock::now();
		// macierz 2 wymiarowa, gdzie kolumny i wiersze to wierzchołki krawędzi
		int graphPrim[VX][VX] = 
				 { {0, 1, 2, 2, 6},
					{1, 0, 4, 0, 5},
					{2, 4, 0, 3, 0},
					{2, 0, 3, 0, 3},
					{6, 5, 0, 3, 0} };

		// Print the solution 
		primMST(graphPrim);
		cout << "Weight of MST is " << cost << endl;
		auto finish = chrono::high_resolution_clock::now();
		chrono::duration<double> elapsed = finish - start;
		cout << "Elapsed time: " << elapsed.count() << " s" << endl;
	}

	system("pause");
	return 0;
}