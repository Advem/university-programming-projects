﻿#include <iostream>
#include <cmath>
#include <fstream>
#include <time.h>

using namespace std;

struct liczba_pierwsza {
	int wartosc; // wartosc liczby pierwszej
	liczba_pierwsza *nastepna;    // wskaznik na następny element
	liczba_pierwsza();            // konstruktor
};

liczba_pierwsza::liczba_pierwsza() {
	nastepna = 0;       // konstruktor
}

struct lista {
	liczba_pierwsza *pierwsza;  // wskaznik na początek listy
	void dodaj_liczbe(int wartosc); // funkcja dodawanie liczby
	void wyswietl_liste(); // funcka wyswietlania listy
	lista();
};

lista::lista() {
	pierwsza = 0;       // konstruktor
}

void lista::dodaj_liczbe(int wartosc)
{
	liczba_pierwsza *nowa = new liczba_pierwsza;    // tworzy nowy element listy

	// wypelniamy danymi
	nowa->wartosc = wartosc;

	if (pierwsza == 0) // sprawdzamy czy to pierwszy element listy
	{
		// jeżeli tak to nowy element jest teraz początkiem listy
		pierwsza = nowa;
	}

	else
	{
		// w przeciwnym wypadku wedrujemy na koniec listy
		liczba_pierwsza *temp = pierwsza;

		while (temp->nastepna)
		{
			// znajdujemy wskaznik na ostatni element
			temp = temp->nastepna;
		}

		temp->nastepna = nowa;  // ostatni element wskazuje na nasz nowy
		nowa->nastepna = 0;     // ostatni nie wskazuje na nic
	}
}

void lista::wyswietl_liste()
{
	// wskaznik na pierszy element listy
	liczba_pierwsza *temp = pierwsza;

	// przewijamy wskazniki na nastepne elementy
	while (temp)
	{
		cout << "wartosc: " << temp->wartosc << endl;
		temp = temp->nastepna;
	}
}

int main()
{
	// tworzymy nowa liste o  nazwie liczby_pierwsze
	lista *liczby_pierwsze = new lista;
	unsigned int  i, w;
	int n = 0;
	bool * S;
	double nSqrt;
	string nazwaPliku = "pierwsze.txt";

	cout << "SitoEratostenesa" << endl << endl;

	// podajemy liczbe wieksza od 2
	do {
		cout << "podaj liczbe n: ";
		cin >> n;
	} while (n <= 2);

	// obliczamy pierwiastek z liczby n
	nSqrt = sqrt(n);
	cout << endl;
	cout << "pierwiastek(" << n << ") = " << nSqrt << endl << endl;

	// tworzymy zmienna logiczna
	S = new bool[n + 1];

	// ustawiamy wartosc zmiennych S na prawde (true)
	for (i = 2; i <= n; i++) S[i] = true;

	// obliczamy  pierwiastek z liczby n z przybliczeniem do liczby calkowitej
	nSqrt = (unsigned int)sqrt(n);
	cout << "pierwiastek_int(" << n << ") = " << nSqrt << endl << endl;

	// rozpoczynamy odliczanie zegara w celu pomiaru szybkosci algorytmu
	clock_t tStart = clock();

	// algorytm Sita sprawdzajacy czy kolejna liczba zbioru liczb calkowitych [2,n] jest liczba pierwsza
	for (i = 2; i <= nSqrt; i++)
	{
		if (S[i])
		{
			w = i * i;
			while (w <= n)
			{
				S[w] = false; w += i;
			}
		}
	}

	// drukujemy czas jaki upłynal algorytmowi na wykonanie obliczen
	printf("Czas wykonania algorytmu: %.10fs\n\n", (double)(clock() - tStart) / CLOCKS_PER_SEC);

	// towrzymy plik w ktorym zapiszemy czas jaki upłynal algorytmowi na wykonanie obliczen
	fstream outfile;
	outfile.open("zlozonosc.txt", ios_base::app);
	if (!outfile.good()) 	cout << "Nie mozna otworzyc pliku" << endl;
	outfile << "dla ";
	outfile << n;
	outfile << " czas: ";
	outfile << ((double)(clock() - tStart) / CLOCKS_PER_SEC);
	outfile << "\n";
	outfile.close();

	// towrzymy plik nazwaPliku
	fstream file;
	file.open(nazwaPliku, ios::out | ios::binary);
	if (!file.good())
	{
		cout << "Nie mozna otworzyc pliku" << endl;
		return 0;
	}

	// drukujemy tylko liczby pierwsze
	cout << "liczby pierwsze: ";
	for (i = 2; i <= n; i++)
	{
		if (S[i])
		{
			cout << i << " "; // zakomentowac jesli za dlugo drukuje
			// dodajemy liczbe i do naszej listy liczby_pierwsze poprzez funkcje dodaj_liczbe
			liczby_pierwsze->dodaj_liczbe(i);
			
			// zapisujemy liczbe i do pliku nazwaPliku
			file << i;
			file << "\n";
		}
	}
	cout << endl;

	// zamykamy plik nazwaPliku
	file.close();

	// usuwamy tablice zmiennych logicznych S
	delete[] S;

	// drukujemy wszystkie liczby z naszej listy liczby_pierwsze za pomoca funkcji wyswietl_liste
	cout << endl << "lista liczb pierwszych ze zbioru [2, " << n << "]: " << endl;
	liczby_pierwsze->wyswietl_liste(); // zakomentowac jesli za dlugo drukuje

	// usuwamy z pamieci nasza liste liczby_pierwsze
	delete (liczby_pierwsze);

	cout << endl;
	system("pause");
	return 0;
}