//...Dnia 22 marzec 2018 o 03:32 Adam Drabik <s169677@student.pg.edu.pl>

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>

using namespace std;

void PW(vector<int> &swap)
{
	vector<int> kopia = swap;
	swap[0] = kopia[1];
	swap[1] = kopia[5];
	swap[2] = kopia[2];
	swap[3] = kopia[0];
	swap[4] = kopia[3];
	swap[5] = kopia[7];
	swap[6] = kopia[4];
	swap[7] = kopia[6];
}

void PO(vector<int> &swap)
{
	vector<int> kopia = swap;
	swap[0] = kopia[3];
	swap[1] = kopia[0];
	swap[2] = kopia[2];
	swap[3] = kopia[4];
	swap[4] = kopia[6];
	swap[5] = kopia[1];
	swap[6] = kopia[7];
	swap[7] = kopia[5];
}

void P10(vector<int> &swap)
{
	vector<int> kopia = swap;
	swap[0] = kopia[2];
	swap[1] = kopia[4];
	swap[2] = kopia[1];
	swap[3] = kopia[6];
	swap[4] = kopia[3];
	swap[5] = kopia[9];
	swap[6] = kopia[0];
	swap[7] = kopia[8];
	swap[8] = kopia[7];
	swap[9] = kopia[5];
}

void P10w8(vector<int> &swap)
{
	vector<int> kopia = swap;
	swap[0] = kopia[5];
	swap[1] = kopia[2];
	swap[2] = kopia[6];
	swap[3] = kopia[3];
	swap[4] = kopia[7];
	swap[5] = kopia[4];
	swap[6] = kopia[9];
	swap[7] = kopia[8];
}

void P4(vector<int> &swap)
{
	vector<int> kopia = swap;
	swap[0] = kopia[1];
	swap[1] = kopia[3];
	swap[2] = kopia[2];
	swap[3] = kopia[0];
}

void P4w8(vector<int> &swap)
{
	vector<int> kopia = swap;
	swap[0] = kopia[3];
	swap[1] = kopia[0];
	swap[2] = kopia[1];
	swap[3] = kopia[2];
	swap[4] = kopia[1];
	swap[5] = kopia[2];
	swap[6] = kopia[3];
	swap[7] = kopia[0];
}

void SL1(vector<int> &swap)
{
	vector<int> kopia = swap;
	swap[0] = kopia[1];
	swap[1] = kopia[2];
	swap[2] = kopia[3];
	swap[3] = kopia[4];
	swap[4] = kopia[0];
}

void SL2(vector<int> &swap)
{
	vector<int> kopia = swap;
	swap[0] = kopia[2];
	swap[1] = kopia[3];
	swap[2] = kopia[4];
	swap[3] = kopia[0];
	swap[4] = kopia[1];
}

void SBox1(vector<int> swap, vector<int> &vec)
{
	int tab[4][4] = {{1,0,3,2}, {3,2,1,0}, {0,2,1,3}, {3,1,3,2}};
	int kolumna = swap[0] * 2 + swap[3] * 1;
	int wiersz = swap[1] * 2 + swap[2] * 1;
	int wskaznik = tab[kolumna][wiersz];

	if (wskaznik == 0) 
	{
		vec.push_back(0);
		vec.push_back(0);
	}

	else if (wskaznik == 1)
	{
		vec.push_back(0);
		vec.push_back(1);
	}

	else if (wskaznik == 2)
	{
		vec.push_back(1);
		vec.push_back(0);
	}

	else if (wskaznik == 3)
	{
		vec.push_back(1);
		vec.push_back(1);
	}
}

void SBox2(vector<int> swap, vector<int> &vec)
{
	int tab[4][4] = { { 0,1,2,3 },{ 2,0,1,3 },{ 3,0,1,0 },{ 2,1,0,3 } };
	int kolumna = swap[0] * 2 + swap[3] * 1;
	int wiersz = swap[1] * 2 + swap[2] * 1;
	int wskaznik = tab[kolumna][wiersz];

	if (wskaznik == 0)
	{
		vec.push_back(0);
		vec.push_back(0);
	}

	else if (wskaznik == 1)
	{
		vec.push_back(0);
		vec.push_back(1);
	}

	else if (wskaznik == 2)
	{
		vec.push_back(1);
		vec.push_back(0);
	}

	else if (wskaznik == 3)
	{
		vec.push_back(1);
		vec.push_back(1);
	}
}

vector<int> Xor(vector<int> v1, vector<int> v2)
{
	vector<int> vec;
	for (int i = 0; i < v1.size(); i++)
	{
		if (v1[i] == v2[i]) vec.push_back(0);
		else vec.push_back(1);
	}

	return vec;
}


int main()
{
	int bit, wybor;
	vector<int> text;
	vector<int> k_p {1,1,0,0,0,0,0,0,1,1};
	vector<int> k_0_0 {0,0,0,0,0};
	vector<int> k_1_0 {0,0,0,0,0};
	vector<int> k_1 {0,0,0,0,0,0,0,0,0,0};
	vector<int> k_2 {0,0,0,0,0,0,0,0,0,0};
	vector<int> t_1 {0,0,0,0};
	vector<int> t_2 {0,0,0,0,0,0,0,0};
	vector<int> t_2_kopia {0,0,0,0};
	vector<int> xor_t_2;
	vector<int> xor_t_2_part1 {0,0,0,0};
	vector<int> xor_t_2_part2 {0,0,0,0};
	vector<int> sbox;
	vector<int> xor_t_1;
	vector<int> szyfr_runda1 {0,0,0,0,0,0,0,0};
	vector<int> szyfr_runda2 {0,0,0,0,0,0,0,0};

	cout << "1. - SZYFROWANIE S-DES\n2. - DESZYFROWANIE S-DES" << endl;
	cout << "Wybierz: ";
	cin >> wybor;
	cout << endl;

	if (wybor == 1)
	{

		// WPROWADZENIE TEKSTU
		cout << "\tWPROWADZENIE TEKSTU" << endl;

		/// 1.1.0
		cout << "Podaj max 8bit tekst odzielony spacjami: ";

		while (cin >> bit) if (bit == 0 or bit == 1) text.push_back(bit);
		while (text.size() != 8) text.push_back(0);

		cout << "Tekst(t): "; for (int i = 0; i < text.size(); i++) cout << text[i];
		cout << endl;

		/// 1.1.1
		PW(text);

		cout << "PW(t): "; for (int i = 0; i < text.size(); i++) cout << text[i];
		cout << endl;

		// GENEREACJA KLUCZY
		cout << endl << "\tGENERACJA KLUCZY" << endl;

		cout << "Klucz(k_p): "; for (int i = 0; i < k_p.size(); i++) cout << k_p[i];
		cout << endl;

		/// 1.2.1
		P10(k_p);

		cout << "P10(k_p): "; for (int i = 0; i < k_p.size(); i++) cout << k_p[i];
		cout << endl;

		/// 1.2.2
		for (int i = 0; i < 5; i++)
		{
			k_0_0[i] = k_p[i];
			k_1_0[i] = k_p[i + 5];
		}

		cout << "k_0_0: "; for (int i = 0; i < k_0_0.size(); i++) cout << k_0_0[i];
		cout << endl;

		cout << "k_1_0: "; for (int i = 0; i < k_1_0.size(); i++) cout << k_1_0[i];
		cout << endl;

		/// 1.2.3
		SL1(k_0_0);
		SL1(k_1_0);

		cout << "SL1(k_0_0): "; for (int i = 0; i < k_0_0.size(); i++) cout << k_0_0[i];
		cout << endl;

		cout << "SL1(k_1_0): "; for (int i = 0; i < k_1_0.size(); i++) cout << k_1_0[i];
		cout << endl;

		/// 1.2.4
		for (int i = 0; i < 5; i++)
		{
			k_1[i] = k_0_0[i];
			k_1[i + 5] = k_1_0[i];
		}

		cout << "SL1+SL1: "; for (int i = 0; i < k_1.size(); i++) cout << k_1[i];
		cout << endl;

		P10w8(k_1);

		k_1.erase(k_1.begin() + 9);
		k_1.erase(k_1.begin() + 8);

		cout << "k_1: "; for (int i = 0; i < k_1.size(); i++) cout << k_1[i];
		cout << endl;

		/// 1.2.5
		SL2(k_1_0);

		for (int i = 0; i < 5; i++)
		{
			k_2[i] = k_0_0[i];
			k_2[i + 5] = k_1_0[i];
		}

		SL2(k_2);

		P10w8(k_2);

		k_2.erase(k_2.begin() + 9);
		k_2.erase(k_2.begin() + 8);

		cout << "k_2: "; for (int i = 0; i < k_2.size(); i++) cout << k_2[i];
		cout << endl;

		// SZYFROWANIE KLUCZEM 1 RUNDY
		cout << endl << "\tSZYFROWANIE KLUCZEM 1 RUNDY" << endl;

		/// 1.3.1
		for (int i = 0; i < 4; i++)
		{
			t_1[i] = text[i];
			t_2[i] = text[i + 4];
			t_2_kopia[i] = text[i + 4];
		}

		cout << "t_1: "; for (int i = 0; i < t_1.size(); i++) cout << t_1[i];
		cout << endl;

		cout << "t_2: "; for (int i = 0; i < t_2.size() - 4; i++) cout << t_2[i];
		cout << endl;

		/// 1.3.2
		P4w8(t_2);

		cout << "P4w8(t_2): "; for (int i = 0; i < t_2.size(); i++) cout << t_2[i];
		cout << endl;

		/// 1.3.3
		xor_t_2 = Xor(t_2, k_1);

		cout << "Xor(t_2, k_1): "; for (int i = 0; i < xor_t_2.size(); i++) cout << xor_t_2[i];
		cout << endl;

		/// 1.3.4
		for (int i = 0; i < 4; i++)
		{
			xor_t_2_part1[i] = xor_t_2[i];
			xor_t_2_part2[i] = xor_t_2[i + 4];
		}

		SBox1(xor_t_2_part1, sbox);
		SBox2(xor_t_2_part2, sbox);

		cout << "SBox(xor_t_2_part1, xor_t_2_part2): ";
		for (auto x : sbox) cout << x;
		cout << endl;

		/// 1.3.5
		P4(sbox);
		cout << "P4(SBox): ";
		for (auto x : sbox) cout << x;
		cout << endl;

		/// 1.3.6
		xor_t_1 = Xor(t_1, sbox);
		cout << "Xor(t_1, SBox): ";
		for (auto x : xor_t_1) cout << x;
		cout << endl;

		/// 1.3.7
		for (int i = 0; i < 4; i++)
		{
			szyfr_runda1[i] = xor_t_1[i];
			szyfr_runda1[i + 4] = t_2_kopia[i];
		}

		cout << "Szyfr kluczem rundy 1: ";
		for (auto x : szyfr_runda1) cout << x;
		cout << endl;

		// SZYFROWANIE KLUCZEM RUNDY 2

		/* wszytsko tak samo ale:
			t_1 to druga czesc szyfr_runda1
			t_2 to pierwsza czesc szyfr_runda1
			kluczem roundy jesy k_2 a nie k_1
		*/

		sbox.clear(); // 2h szukania bledu

		cout << endl << "\tSZYFROWANIE KLUCZEM 2 RUNDY" << endl;

		/// 1.3.1
		for (int i = 0; i < 4; i++)
		{
			t_1[i] = szyfr_runda1[i + 4];
			t_2[i] = szyfr_runda1[i];
			t_2_kopia[i] = szyfr_runda1[i];
		}

		cout << "t_1: "; for (int i = 0; i < t_1.size(); i++) cout << t_1[i];
		cout << endl;

		cout << "t_2: "; for (int i = 0; i < t_2.size() - 4; i++) cout << t_2[i];
		cout << endl;

		/// 1.3.2
		P4w8(t_2);

		cout << "P4w8(t_2): "; for (int i = 0; i < t_2.size(); i++) cout << t_2[i];
		cout << endl;

		/// 1.3.3
		xor_t_2 = Xor(t_2, k_2);

		cout << "Xor(t_2, k_2): "; for (int i = 0; i < xor_t_2.size(); i++) cout << xor_t_2[i];
		cout << endl;

		/// 1.3.4
		for (int i = 0; i < 4; i++)
		{
			xor_t_2_part1[i] = xor_t_2[i];
			xor_t_2_part2[i] = xor_t_2[i + 4];
		}

		SBox1(xor_t_2_part1, sbox);
		SBox2(xor_t_2_part2, sbox);

		cout << "SBox(xor_t_2_part1, xor_t_2_part2): ";
		for (auto x : sbox) cout << x;
		cout << endl;

		/// 1.3.5
		P4(sbox);
		cout << "P4(SBox): ";
		for (auto x : sbox) cout << x;
		cout << endl;

		/// 1.3.6
		xor_t_1 = Xor(t_1, sbox);
		cout << "Xor(t_1, SBox): ";
		for (auto x : xor_t_1) cout << x;
		cout << endl;

		/// 1.3.7
		for (int i = 0; i < 4; i++)
		{
			szyfr_runda2[i] = xor_t_1[i];
			szyfr_runda2[i + 4] = t_2_kopia[i];
		}

		cout << "Szyfr kluczem rundy 2: ";
		for (auto x : szyfr_runda2) cout << x;
		cout << endl;

		// ALGORYTM KONCZACY
		cout << endl << "\tALGORYTM KONCZACY" << endl;

		PO(szyfr_runda2);

		cout << "PO(szyfr_runda2): ";
		for (auto x : szyfr_runda2) cout << x;
	}

	if (wybor == 2)
	{

		// WPROWADZENIE TEKSTU
		cout << "\tWPROWADZENIE TEKSTU" << endl;

		/// 1.1.0
		cout << "Podaj max 8bit tekst odzielony spacjami: ";

		while (cin >> bit) if (bit == 0 or bit == 1) text.push_back(bit);
		while (text.size() != 8) text.push_back(0);

		cout << "Tekst(t): "; for (int i = 0; i < text.size(); i++) cout << text[i];
		cout << endl;

		/// 1.1.1
		PW(text);

		cout << "PW(t): "; for (int i = 0; i < text.size(); i++) cout << text[i];
		cout << endl;

		// GENEREACJA KLUCZY
		cout << endl << "\tGENERACJA KLUCZY" << endl;

		cout << "Klucz(k_p): "; for (int i = 0; i < k_p.size(); i++) cout << k_p[i];
		cout << endl;

		/// 1.2.1
		P10(k_p);

		cout << "P10(k_p): "; for (int i = 0; i < k_p.size(); i++) cout << k_p[i];
		cout << endl;

		/// 1.2.2
		for (int i = 0; i < 5; i++)
		{
			k_0_0[i] = k_p[i];
			k_1_0[i] = k_p[i + 5];
		}

		cout << "k_0_0: "; for (int i = 0; i < k_0_0.size(); i++) cout << k_0_0[i];
		cout << endl;

		cout << "k_1_0: "; for (int i = 0; i < k_1_0.size(); i++) cout << k_1_0[i];
		cout << endl;

		/// 1.2.3
		SL1(k_0_0);
		SL1(k_1_0);

		cout << "SL1(k_0_0): "; for (int i = 0; i < k_0_0.size(); i++) cout << k_0_0[i];
		cout << endl;

		cout << "SL1(k_1_0): "; for (int i = 0; i < k_1_0.size(); i++) cout << k_1_0[i];
		cout << endl;

		/// 1.2.4
		for (int i = 0; i < 5; i++)
		{
			k_1[i] = k_0_0[i];
			k_1[i + 5] = k_1_0[i];
		}

		cout << "SL1+SL1: "; for (int i = 0; i < k_1.size(); i++) cout << k_1[i];
		cout << endl;

		P10w8(k_1);

		k_1.erase(k_1.begin() + 9);
		k_1.erase(k_1.begin() + 8);

		cout << "k_1: "; for (int i = 0; i < k_1.size(); i++) cout << k_1[i];
		cout << endl;

		/// 1.2.5
		SL2(k_1_0);

		for (int i = 0; i < 5; i++)
		{
			k_2[i] = k_0_0[i];
			k_2[i + 5] = k_1_0[i];
		}

		SL2(k_2);

		P10w8(k_2);

		k_2.erase(k_2.begin() + 9);
		k_2.erase(k_2.begin() + 8);

		cout << "k_2: "; for (int i = 0; i < k_2.size(); i++) cout << k_2[i];
		cout << endl;

		// SZYFROWANIE KLUCZEM 2 RUNDY
		cout << endl << "\tSZYFROWANIE KLUCZEM 2 RUNDY" << endl;

		/// 1.3.1
		for (int i = 0; i < 4; i++)
		{
			t_1[i] = text[i];
			t_2[i] = text[i + 4];
			t_2_kopia[i] = text[i + 4];
		}

		cout << "t_1: "; for (int i = 0; i < t_1.size(); i++) cout << t_1[i];
		cout << endl;

		cout << "t_2: "; for (int i = 0; i < t_2.size() - 4; i++) cout << t_2[i];
		cout << endl;

		/// 1.3.2
		P4w8(t_2);

		cout << "P4w8(t_2): "; for (int i = 0; i < t_2.size(); i++) cout << t_2[i];
		cout << endl;

		/// 1.3.3
		xor_t_2 = Xor(t_2, k_2); //tutaj

		cout << "Xor(t_2, k_2): "; for (int i = 0; i < xor_t_2.size(); i++) cout << xor_t_2[i];
		cout << endl;

		/// 1.3.4
		for (int i = 0; i < 4; i++)
		{
			xor_t_2_part1[i] = xor_t_2[i];
			xor_t_2_part2[i] = xor_t_2[i + 4];
		}

		SBox1(xor_t_2_part1, sbox);
		SBox2(xor_t_2_part2, sbox);

		cout << "SBox(xor_t_2_part1, xor_t_2_part2): ";
		for (auto x : sbox) cout << x;
		cout << endl;

		/// 1.3.5
		P4(sbox);
		cout << "P4(SBox): ";
		for (auto x : sbox) cout << x;
		cout << endl;

		/// 1.3.6
		xor_t_1 = Xor(t_1, sbox);
		cout << "Xor(t_1, SBox): ";
		for (auto x : xor_t_1) cout << x;
		cout << endl;

		/// 1.3.7
		for (int i = 0; i < 4; i++)
		{
			szyfr_runda1[i] = xor_t_1[i];
			szyfr_runda1[i + 4] = t_2_kopia[i];
		}

		cout << "Szyfr kluczem rundy 2: ";
		for (auto x : szyfr_runda1) cout << x;
		cout << endl;

		// SZYFROWANIE KLUCZEM RUNDY 1

		/* wszytsko tak samo ale:
		t_1 to druga czesc szyfr_runda1
		t_2 to pierwsza czesc szyfr_runda1
		kluczem roundy jesy k_1 a nie k_2
		*/

		sbox.clear(); // 2h szukania bledu

		cout << endl << "\tSZYFROWANIE KLUCZEM 1 RUNDY" << endl;

		/// 1.3.1
		for (int i = 0; i < 4; i++)
		{
			t_1[i] = szyfr_runda1[i + 4];
			t_2[i] = szyfr_runda1[i];
			t_2_kopia[i] = szyfr_runda1[i];
		}

		cout << "t_1: "; for (int i = 0; i < t_1.size(); i++) cout << t_1[i];
		cout << endl;

		cout << "t_2: "; for (int i = 0; i < t_2.size() - 4; i++) cout << t_2[i];
		cout << endl;

		/// 1.3.2
		P4w8(t_2);

		cout << "P4w8(t_2): "; for (int i = 0; i < t_2.size(); i++) cout << t_2[i];
		cout << endl;

		/// 1.3.3
		xor_t_2 = Xor(t_2, k_1); //tutaj

		cout << "Xor(t_2, k_1): "; for (int i = 0; i < xor_t_2.size(); i++) cout << xor_t_2[i];
		cout << endl;

		/// 1.3.4
		for (int i = 0; i < 4; i++)
		{
			xor_t_2_part1[i] = xor_t_2[i];
			xor_t_2_part2[i] = xor_t_2[i + 4];
		}

		SBox1(xor_t_2_part1, sbox);
		SBox2(xor_t_2_part2, sbox);

		cout << "SBox(xor_t_2_part1, xor_t_2_part2): ";
		for (auto x : sbox) cout << x;
		cout << endl;

		/// 1.3.5
		P4(sbox);
		cout << "P4(SBox): ";
		for (auto x : sbox) cout << x;
		cout << endl;

		/// 1.3.6
		xor_t_1 = Xor(t_1, sbox);
		cout << "Xor(t_1, SBox): ";
		for (auto x : xor_t_1) cout << x;
		cout << endl;

		/// 1.3.7
		for (int i = 0; i < 4; i++)
		{
			szyfr_runda2[i] = xor_t_1[i];
			szyfr_runda2[i + 4] = t_2_kopia[i];
		}

		cout << "Szyfr kluczem rundy 1: ";
		for (auto x : szyfr_runda2) cout << x;
		cout << endl;

		// ALGORYTM KONCZACY
		cout << endl << "\tALGORYTM KONCZACY" << endl;

		PO(szyfr_runda2);

		cout << "PO(szyfr_runda1): ";
		for (auto x : szyfr_runda2) cout << x;
	}

	cout << endl << endl;
	system("PAUSE");
	return 0;
}