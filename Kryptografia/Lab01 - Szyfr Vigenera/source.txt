//...Dnia 15 marzec 2018 o 16:08 Adam Drabik <s169677@student.pg.edu.pl>

//#include "std_lib_facilities.hpp" 
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <windows.h>

using namespace std;

HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE); // kolory

void swap(vector<int> &swap) // przesuniecie a->z
{
vector<int> kopia = swap;
swap[0] = kopia[25];
swap[1] = kopia[0];
swap[2] = kopia[1];
swap[3] = kopia[2];
swap[4] = kopia[3];
swap[5] = kopia[4];
swap[6] = kopia[5];
swap[7] = kopia[6];
swap[8] = kopia[7];
swap[9] = kopia[8];
swap[10] = kopia[9];
swap[11] = kopia[10];
swap[12] = kopia[11];
swap[13] = kopia[12];
swap[14] = kopia[13];
swap[15] = kopia[14];
swap[16] = kopia[15];
swap[17] = kopia[16];
swap[18] = kopia[17];
swap[19] = kopia[18];
swap[20] = kopia[19];
swap[21] = kopia[20];
swap[22] = kopia[21];
swap[23] = kopia[22];
swap[24] = kopia[23];
swap[25] = kopia[24];

}

int main()
{
int j = 0;
int correct = 0;
int incorrect = 0;
int wartosc, wybor, dlug_klucz;
double suma_wynik = 0;
string klucz, linia, szyfrogram;
fstream tekst;
fstream krypted;
vector<int> count{ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 }; //(26)
vector<int> zlicz(26);

/// Menu
cout << "\tKryptografia Lab1 - Szyfr Vigenere'a " << endl;
cout << endl << "1. Szyfrowanie tekstu" << endl << "2. Deszyfrowanie tekstu" << endl << "3. Obliczenie indeksu zgodnosci" << endl << "4. Obliczenie wspolcznnika koincydencji miedzy kolumanmi" << endl << "Wybierz: ";
cin >> wybor;

/// 1. Szyfrowanie tekstu ///////////////////////////////////////////////////////////
if (wybor == 1)
{

cout << "Podaj klucz szyfrujacy: ";
cin >> klucz;
cout << endl << "Klucz int: ";

/// Konwenterowanie string klucz na int klucz
for (int j = 0; j < klucz.length(); j++)
{
cout << (int)klucz[j] - 97 << " ";
}

cout << endl << endl << "Tekst str: ";


/// Odczytwanie z pliku
tekst.open("tekst.txt", ios::in | ios::out);
if (tekst.good() == true)
{
while (!tekst.eof())
{
getline(tekst, linia);
cout << linia; //wyswietlanie lini tekstu
cout << endl << endl << "Tekst int: ";

/// Konwertowanie tekstu na liczby
for (int i = 0; i < linia.length(); i++)
{
cout << (int)linia[i] - 97 << " ";
}

cout << endl << endl << "Szyfr int: ";

/// Szyfrowanie tekstu poprzez sume z kluczem
for (int i = 0; i < linia.length(); i++)
{
wartosc = (int)linia[i] - 97 + (int)klucz[j] - 97;
if (wartosc >= 26) cout << (int)linia[i] - 97 + (int)klucz[j] - 97 - 26 << " ";
if (wartosc < 26) cout << (int)linia[i] - 97 + (int)klucz[j] - 97 << " ";
j++;
if (j >= klucz.length()) j = 0;
}

cout << endl << endl << "Szyfr str: ";

/// Konwertowanie zaszyfrowanego tekstu z postaci liczb na tekst
for (int i = 0; i < linia.length(); i++)
{
wartosc = (int)linia[i] - 97 + (int)klucz[j] - 97;
if (wartosc >= 26) wartosc -= 26;
j++;
if (j >= klucz.length()) j = 0;
szyfrogram += (char)(97 + wartosc);

}

cout << szyfrogram;
}
tekst.close();
}

/// Zapisywanie zaszyfrowanego tekstu do pliku
krypted.open("krypted.txt", ios::out | ios::trunc);
if (krypted.good() == true)
{
krypted << szyfrogram;
krypted.close();
}
cout << endl;
}

/// 2. Deszyfrowanie tekstu /////////////////////////////////////////////////////////
if (wybor == 2)
{
cout << "Podaj klucz deszyfrujacy: ";
cin >> klucz;
cout << endl << "Klucz int: ";

/// Konwenterowanie string klucz na int klucz
for (int j = 0; j < klucz.length(); j++)
{
cout << (int)klucz[j] - 97 << " ";
}

cout << endl << endl << "Szyfr str: ";


/// Odczytwanie z pliku
krypted.open("krypted.txt", ios::in | ios::out);
if (krypted.good() == true)
{
while (!krypted.eof())
{
getline(krypted, linia);
cout << linia; //wyswietlanie lini tekstu
cout << endl << endl << "Szyfr int: ";

/// Konwertowanie zaszyforwanego tekstu na liczby
for (int i = 0; i < linia.length(); i++)
{
cout << (int)linia[i] - 97 << " ";
}

cout << endl << endl << "Tekst int: ";

/// Deszyfrowanie tekstu poprzez roznice z kluczem
for (int i = 0; i < linia.length(); i++)
{
wartosc = (int)linia[i] - (int)klucz[j];
if (wartosc >= 26) cout << (int)linia[i] - (int)klucz[j] - 26 << " ";
if (wartosc < 0) cout << (int)linia[i] - (int)klucz[j] + 26 << " ";
if (wartosc < 26 && wartosc >= 0) cout << (int)linia[i] - (int)klucz[j] << " ";
j++;
if (j >= klucz.length()) j = 0;
}

cout << endl << endl << "Tekst str: ";

/// Konwertowanie tekstu z postaci liczb na tekst
for (int i = 0; i < linia.length(); i++)
{
wartosc = (int)linia[i] - (int)klucz[j];
if (wartosc >= 26) wartosc -= 26;
//if (wartosc >= 52) wartosc -= 52; ????
if (wartosc < 0) wartosc += 26;
j++;
if (j >= klucz.length()) j = 0;
szyfrogram += (char)(97 + wartosc);

}

cout << szyfrogram;
}
tekst.close();
}

/// Zapisywanie zaszyfrowanego tekstu do pliku
tekst.open("tekst.txt", ios::out | ios::trunc);
if (tekst.good() == true)
{
tekst << szyfrogram;
tekst.close();
}

cout << endl;
}

/// 3. Obliczenie indeksu zgodnosci //////////////////////////////////////////////////
if (wybor == 3)
{
cout << "Podaj dlugosc klucza: ";
cin >> dlug_klucz;
cout << endl << "Szyfr str: ";

/// Odczytwanie z pliku
krypted.open("krypted.txt", ios::in | ios::out);
if (krypted.good() == true)
{
while (!krypted.eof())
{
getline(krypted, linia);
cout << linia; //wyswietlanie lini tekstu
cout << endl;

double suma_elementow = 0;
for (int i = 0; i < linia.length(); i++)
{
if (i % dlug_klucz == 0)
{
count[linia[i] - 97]++;
suma_elementow++;
}
}

/// Zliczanie liter
double suma = 0; // epsilon
double indeks = 0;
char litera;
cout << endl;

for (int i = 0; i < linia.length(); i++)
{
for (int j = 0; j <= 25; j++)
{
if ((int)linia[i] - 97 == j) zlicz[j] += 1;
}
}

for (int j = 0; j <= 25; j++)
{
litera = j + 97;
cout << "Litera: " << litera << " wystapila " << zlicz[j] << " razy." << endl;
suma += count[j] * (count[j] - 1);
}

indeks = suma / (suma_elementow*(suma_elementow - 1));
cout << endl << "N wynosi: " << suma_elementow << endl;
cout << "Suma wynosi: " << suma << endl;
cout << "Indeks dla jednej kolumny wynosi: " << indeks << endl;

//...
}
tekst.close();
}

}

/// 4. Obliczenie indeksu zgodnosci dla wielu kolumn /////////////////////////////////
if (wybor == 4)
{
cout << "Podaj dlugosc klucza: ";
cin >> dlug_klucz;
cout << endl;
/// Odczytwanie z pliku
krypted.open("krypted.txt", ios::in | ios::out);
if (krypted.good() == true)
{
while (!krypted.eof())
{
getline(krypted, linia);
cout << linia; //wyswietlanie lini tekstu

double suma_elementow = 0;
for (int i = 0; i < linia.length(); i++)
{
if (i % dlug_klucz == 0)
{
count[linia[i] - 97]++;
suma_elementow++;
}
}

/// Zliczanie liter
double suma = 0; // epsilon
double indeks = 0;
char litera;
cout << endl << endl;

for (int i = 0; i < linia.length(); i++)
{
for (int j = 0; j <= 25; j++)
{
if ((int)linia[i] - 97 == j) zlicz[j] += 1;
}
}

for (int j = 0; j <= 25; j++)
{
litera = j + 97;
cout << "Litera: " << litera << " wystapila " << zlicz[j] << " razy." << endl;
suma += count[j] * (count[j] - 1);
}

indeks = suma / (suma_elementow*(suma_elementow - 1));
cout << endl << "N wynosi: " << suma_elementow << endl;
cout << "Suma wynosi: " << suma << endl;
cout << "Indeks dla jednej kolumny wynosi: " << indeks << endl;

for (int i = 0; i < dlug_klucz - 1; i++)
{
vector<int> kolumna1{ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
double n1 = 0; // ilosc elementow w pierwszej kolumnie

for (int j = 0; j < linia.length(); j++)
{
if (j % dlug_klucz == i)
{
kolumna1[linia[j] - 97]++;
n1++;
}
}

for (int j = i + 1; j < dlug_klucz; j++)
{
vector<int> kolumna2{ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
double n2 = 0; // ilosc elementow w drugiej kolumnie

for (int k = 0; k < linia.length(); k++)
{
if (k % dlug_klucz == j)
{
kolumna2[linia[k] - 97]++;
n2++;
}
}

double wynik;
int error = 0;
do
{
double suma_ilocz = 0; //suma_iloczynu_elementu_pomiedzy_kolumanmi
for (int k = 0; k <= 25; k++)
{
suma_ilocz += kolumna1[k] * kolumna2[k];
}
wynik = suma_ilocz / (n1*n2);
error++;
swap(kolumna2);
//cout << error << " " << endl;
} while ((wynik <= 0.06 || wynik >= 0.07) && error < 26);

cout << "Wspolczynnik koincydencji miedzy kolumna " << i + 1 << " a " << j + 1 << " wynosi: " << setprecision(5) << wynik;
suma_wynik += wynik;

if (wynik > 0.06 && wynik < 0.07)
{
SetConsoleTextAttribute(hStdOut, FOREGROUND_GREEN | FOREGROUND_GREEN | FOREGROUND_INTENSITY);
cout << " OK!" << endl;
correct++;
SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 7);
}
else
{
SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 4);
cout << " NO!" << endl;
incorrect++;
SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 7);
}
}

}
}
tekst.close();
}
cout << endl << "Poprawnych koincydencji: " << correct << "/" << correct + incorrect;
if (correct >= incorrect) SetConsoleTextAttribute(hStdOut, FOREGROUND_GREEN | FOREGROUND_GREEN | FOREGROUND_INTENSITY);
else SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 4);
cout << " (" << 100 * correct / (correct + incorrect) << "%)" << endl;
SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 7);
cout << "Sredni wspolczynnik: ";
if (suma_wynik / (correct + incorrect) > 0.061 && suma_wynik / (correct + incorrect) < 0.069) SetConsoleTextAttribute(hStdOut, FOREGROUND_GREEN | FOREGROUND_GREEN | FOREGROUND_INTENSITY);
else SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 4);
cout << suma_wynik / (correct + incorrect) << endl;
SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 7);
if (suma_wynik / (correct + incorrect) > 0.061 && suma_wynik / (correct + incorrect) < 0.069)
{
cout << "Prawdopodobna dlugosc klucza: ";
SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 9);
cout << dlug_klucz << endl;
SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 7);
}
}

cout << endl;
system("PAUSE");
return 0;
}