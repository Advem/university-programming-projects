# Stochastyczne Programowanie Dynamiczne
#### Implementacja algorytmu w języku Julia
#### Adam Drabik

## 1. Czym jest Stochastyczne Programowanie Dynamiczne (Stochastic Dynamic Programming)

Stochastyczne programowanie dynamiczne jest techniką modelowania i rozwiązywania problemów związanych z podejmowaniem decyzji w warunkach niepewności 
początkowo wprowadzona przez Richarda E. Bellmana w 1957 roku. Metoda ta jest ściśle związana z programowaniem stochastycznym oraz programowaniem dynamicznym, 
które reprezentują problem w postaci równania Bellmana, którego celem jest obliczenie najbardziej optymalnego działania w obliczu niepewności.

Stochastyczne programy dynamiczne można rozwiązać optymalnie, stosując rekurencję wsteczną lub algorytm rekursji do przodu.
W celu zwiększenia wydajności stosuje się technikę zapamiętywania, która polega na przechowywaniu pewnych wyników w pamięci 
i odzyskiwania ich, gdy dane wejściowe powtarzają się.

## 2. Przykład zastosowania algorytmu

Algorytm został zaimplementowany na przykładzie gry hazardowej, w której gracz zostaje poinformowany o prawdopobieństwie wygranej pewnej ilości 
pieniędzy po pewnej ilości gier, biorąc pod uwagę stan finansowy gracza oraz możliwe opcje zakładu w każdej turze rozgrywki.
Gracz rozpoczyna grę z pewno ilością pieniędzy oraz ustala ile tur chce zagrać. Co turę gracz obstawia zakład pewną liczbę pieniędzy po czym dochodzi do losowania.
Gracz ma wcześniej określoną, stałą szansę na wygraną i w momencie, gdy tura przebiegła pozytywnie, graczowi zwracana jest złożona kwota zakładu oraz dodatkowe wynagrodzenie, równe ilości zakładu. Jeżeli gracz przegra turę, traci kwotę zakładu.
Celem gracza jest uzyskanie określonego zysku po określonej liczbie gier, a za pomocą algorytmu, gracz może dowiedzieć się jaką ma szansę na wygranie całej rozgrywki, dzięki czemu może rozsądniej dokonywać zakładów.

### Wprowadzenie do rozgrywki:

Na początku należy zdefiniować pewne wartości ustalające zasady gry za pomocą stałych
- winChance - procentowe prawdopodobieństwo na wygraną danej tury, przyjmujące wartość od 0 do 1 (upraszczając przykład, prawdopodobieństwo na wygraną danej truy jest stała przez całą rozgrywkę)
- loseChance - procentowe prawdopodobieństwo na przegraną danej tury, obliczane poprzez substrakcję szansy na wygranej od wartości 1
- games - liczba określająca ilość tury rozgrywki, po których gra zostanie zakończona. Wartość ta musi zostać wcześniej określona i pozostać niezmienna przez czas rozgrywki, gyż jest istota do obliczenia predyckji wyniku następnych gier
- initialMoney - liczba określająca kwotę z jaką gracz rozpoczyna rozgrywkę
- makeMoney - liczba pieniędzy porządana przez gracza, której osiągnięcie jest celem rozgrywki

Istotnymi również zmiennymi są:
- game - numer aktualnej rozgrywki, który symbolizuje przebieg rozgrywki, a wyrównanie wartości tej zmiennej ze zmienną $games sygnalizuje zakończenie rozgrywki ze skutkiem niewiadomym
- playerMoney - aktualny stan finansowy gracza, którego wartość równa wartości zmiennej $makeMoney jest porządana, a uzyskanie lub jej przekroczenie sygnalizuje zakończenie rozgrywki ze skutkiem pozytywnym 
- bet - kwota zakładu jaką gracz decyduje przeznaczyć na daną turę, której wartość gracz traci przed losowaniem wyniku tury. W momencie wygrania tury przez gracza, otrzymuje on zwrot kwoty zakładu oraz dodatkową nagrodę pieniężną równą kwocie zakłądu. Jeżeli gracz przegra daną turę, traci on bezpowrotnie kwotę zakładu oraz zostaje rozpoczęta kolejna tura, aż do momentu zakończenia rozgrywki.
- gameWon - określenie wyniku danej tury. W momencie wygrania tury przyjmuje wartośc true, w przeciwnym wypadku - wartość false
- random - losowa liczba, która służy jako wyznacznik decyzji o stanie danej tury

### Przebieg rozgrywki:

0. Definicje stałych (zaday gry)
- Przykładowo ustalmy, że szansa na wygraną gracza w danej turze wynosi 0.4 (40%)
- **winChance = 0.4**
- Analogicznie przawdopodobieństwo na przegraną gracza to 1 - szansa na wygraną
- **loseChance = 1 - winChance = 1 - 0.4 = 0.6**
- Przyjmijmy, że gracz ma do dyspozycji 2$ i pragnie powiększyć swój majątek do 6$ po 4 turach gry
- **initialMoney = 2**
- **makeMoney = 6**
- **games = 4**
- Na samym początku rozpoczynamy od 1 gry, więc ustawiamy zmienną:
- **game = 1** 
- oraz ustalamy, że gracz aktualnie posiada kwotę równą kwocie początkowej
- **playerMoney = initialMoney = 2**

1. Rozpoczęcie pierwszej tury - zakład
- gracz decyduje, jaką postawić kwotę zakładu. Może on wybrać dowolną wartość dodatnią mniejszą lub równą ilości aktualnie posiadanych pieniędzy playerMoney
- gracz obstawia kwotę zakładu **bet = 1**, co powoduję o zmniejszenie aktualnie posiadanych pieniędzy przez gracza o wartość **bet**
- **playerMoney -= bet **
- **playerMoney = 1**

2. Decyzja o stanie tury (wygrana)
- dochodzi do losowania, którego wynik zadecyduje, czy gracz wygra lub czy przegra aktualną turę. Losowana zostaje liczba zienno-przecinkowa z przedziały od 0 do 1.
- jeżeli wylosowana liczba jest mniejsza od prawdopodobieństwa na wygraną - gracz wygrywa turę
- jeżei wylosowana liczba jest większa/równa prawdopodobieństu na wygraną - gracz przegrywa turę
- Gracz ma szansę 40% na wygraną, więc istnieje prawdopodobieństwo równe 0.4, że wylosowana liczba z przedziału od 0 do 1 jest liczbą mniejszą od liczby 0.4 - na tej podstawie decyzja o wygranej turze gracza jest losowa (pseudo-losowa)
- Wylosowana liczba to **random = 0.2378**, która jest mniejsza od **winChace = 0.4"**, więc gra wygrywa daną turę **gameWon = true**
- gracz otrzymuje wynagrodzenie równe kwocie zakładu **bet = 1** oraz zwrot zakładuj **playerMoney += 2*bet**
- **playerMoney = 3**
- tura zakańcza się więc powodzeniem i przechodzimy do następnej tury **game += 1 = 2**

3. Rozpoczęcie kolejnej tury - zakład
- gracz pownownie decyduje, jaką postawić kwotę zakładu na tą turę.
- gracz obstawia kwotę zakładu **bet = 1**, co powoduję o zmniejszenie aktualnie posiadanych pieniędzy przez gracza o wartość **bet**
- **playerMoney -= bet **
- **playerMoney = 2**

4. Decyzja o stanie tury (przegrana)
- dochodzi podownie do losowania liczby, tym razem wylosowana liczba to **random = 0.5478**, która jest większa od liczby **winChance = 0.4**, więc gracz przegrywa ta turę
- **gameWon = false**
- gracz nie odzyskuje kwoty zakładu i przechodzi do kolejnej tury **game += 1 = 3**

5. Dalszy przebieg rozgrywki
- gracz kolejnej turze (3) obstawia kwotę **bet = 2** i wygrywa, posiadając w turze **game = 4** budżet **playerMoney = 4**, więc w ostatniej turze obstawia zakład **bet = 3**, który również wygrywa

6. Zakończenie rozgrywki
- gra kończy się powodzeniem, gdyż wartość liczby tur zrównała się z liczbą ilości zaplanowanych gier **game == games**, a gracz pozydtywnie kończy rozgrywkę posiadając na swoim koncie **playerMoney = 7** co jest większe od porządanego wyniku **makeMoney = 6**

### Algorytm stochastyczny

Za pomocą stochastycznego programowania dynamicznego i poznania wcześniej ustalonych zasad rozgrywki, możemy obliczać prawdopodobieństwo na pozytywne zakończenie całej rozgrywki
i defakto podpowiadać graczowi jaką ma szansę na wygraną wynikającą z decyzji gracza co do ilości zakładów w kolejnych grach.
Dzięki odpowiednim danym można obliczyć prawdopodobieństw wygranych w każdej kolejnej turze za pomocą obliczania rekurencji wtecznej.
Mając informacje o stanie aktualnej rozgrywki możemy w łatwy sposób obliczyć jakie skutki są możliwe do zajścia zależnie od decyzji gracza, natomiast znajdą kolejne i jeszcze dalsze stany rozgrywki, możliwa jest predykcja wyniku gry n-tej już na samym początku rozgrywki.
Można to wykonać za pomocą symulacji wszystkich możliwych scenariuszy, decyzji jakie podejmuje gracz mimo tego, że wyniki kolejnych tur są losowe.
Znając liczbę gier, prawdopodobieństwo wygranej w turze oraz porządaną kwotę końcową, możemy stwierdzić przy jakich warunkach może dojść do wygranej:

- jeżeli gracz w ostatniej turze posiada 0$, 1$ lub 2$ to nie ma on możliwości uzyskania wyniku 6$, ponieważ nie jest on w stanie założyć wystarczająco dużej kwoty zakładu
- jeżeli gracz w ostatniej turze posiada 3$, 4$ lub 5$ ma on szansę równą **winChance = 0.4**, a dodatkowo powinien on założyć jak najmniejszą kwotę zakładu, aby w razie przgranej zbliżyć się jak najliżej, celu tj. uzyskania wartości porządanej
- jeżeli gracz w ostatniej turze posiada 6$ lub więcej, wygrał on defakto całą rozgrywkę i nie powinien obstawiać więcej, gdyż osiągnął on posiadaną kwotę w przestrzeni wybranej liczbie gier

posiadając informację o prawdopodobieństwie na wygraną w ostaniej turze, możemy cofnąć się o kolejne tury do tyłu i dokonywać takich samych analiz dla innych stanów finansowych oraz ewentualnych decyzji gracza, aż do momentu pierwszej rozgrywki, informując gracza w jaki sposób należy przeprowadzać rozgrywkę, aby **ZMAKSYMALIZOWAĆ** swój dochód na koniec rozgrywki

### Rekurencja wprzód
Za pomocą rekurencji jesteśmy w stanie szybciej dokonywać operacji, ponieważ jak już wyżej zostało to przedstawione, w celu obliczenia możliwych zajśc tury 1, potrzebujemy informacji z tury kolejnej, aż tak do końca rozgrywki. Z tego faktu, iż decyzje gracza co do kwot zakładów są różne i ich przebieg jest dwuznaczny, stany aktualnej rozgrywki są różne, lecz wciąż policzalne. Skoro do obliczenia tury wcześniejszej w określonych warunkach potrzeujemy informacji z przebiegu kolejnej tury przy określonych warunkach, może dojść do sytualcji, gdzie musimy wykorzystać informacje już wcześniej obliczone. Za pomocą rekurencji wprzód możemy przyśpiszyć czas obliczęń prawdopodobieństw korzystając z wcześniej wykalkulowanych danych.

### Obliczanie prawdopodobieństw
W programie zastosowano metodę tworzeia macierzy trójwymiarowej w celu obliczenia wszystkich możliwych kombinacji stanów rozgrywki. Bardziej optymalną metodą byłoby obliczanie możliwych scenariuszy na bierząco, wykorzystując już wczesniej obliczone dane z innych tur. 
Macierz trójwymiarowa posiada 3 osie współrzędnych, które odpowiadają kolejno: numerowu aktualnej tury **game**, aktualnym finansowym stanie gracza **playerMoney** oraz wartości zakładu, jaką ma zamiar postawić gracz **bet**.
Dla każdej kombinacji tych wartości istnieje inne prawdopodobieństwo na wygraną, które przebiega aż do momentu zakończenia rozgrywki, a proces wynikania danej wartości można przedstawić za pomocą równiania:

*f[i][n][b] = 0.4*f[i+1][n+b][b] + 0.6f[i+1][n-b][b]*

*f[game][playerMoney][bet] = winChance*f[next game][playerMoney + bet][bet] + loseChance*f[next game][playerMoney - bet][bet]*

Szansa *f* na wygranie tury *i* przy aktualnym majątku *n* oraz planowanym zakładzie *b* jest równa sumie [prawdopodobieństwa wygrania *f*(która pomnożona jest o szansę na wygrnie tury *winChance*) tury kolejnej *i + 1* przy posiadanym majątku(majątek poprzedniej tury powiększony o kwotę zakładu poprzedniej tury w wyniku wygrania poprzedniej tury) *n + b* oraz planowanym zakładzie *b*] oraz [prawdopodobieństwa przegrania *f*(która pomnożona jest o szansę na przegranie tury *loseChance*) tury kolejnej *i + 1* przy posiadanym majątku(majątek poprzedniej tury pomniejszony o kwotę zakładu poprzedniej tury w wyniku przegrania poprzedniej tury) *n - b* oraz planowanym zakładzie *b*]]
Jak można zauwzyć, aby obliczyć prawdopodobieństwo wygrania rozgrywki, musimy przekalkulować możliwe scenariusze kolejnych tur, gdzie szansa na powodzenie nasępnej tury jest nieznana, o czym mówi **Stochastyczność**, a decyzje graczą są różne i zmieniają się podczas rozgrywki, o czym mówi "**Dynamiczność**" tej metody programowania.



****